<?php
class Model_images
{
    private $table = "agenda";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function setImage($img){
        $sql = "INSERT INTO iconImages SET tableSrc =:tbl , tableIdx =:idx , imgType = :ity , iconImg =:img";
        $this->db->query($sql);
        $this->db->bind('tbl',$img['tabel']);
        $this->db->bind('idx',$img['nomor']);
        $this->db->bind('ity',$img['imgtp']);
        $this->db->bind('img',$img['image']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function chgImage($img){
        $sql = "UPDATE iconImages SET imgType = :itp , iconImg =:img WHERE tableSrc =:tbl && tableIdx =:idx ";
        $this->db->query($sql);
        $this->db->bind('img',$img['image']);
        $this->db->bind('tbl',$img['tabel']);
        $this->db->bind('itp',$img['imgtp']);
        $this->db->bind('idx',$img['nomor']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function showImage($tab,$idx){
        $sql = "SELECT  iconImg , imgType FROM iconImages WHERE tableSrc =:tabel && tableIdx =:id";
        $this->db->query($sql);
        $this->db->bind('tabel',$tab);
        $this->db->bind('id',$idx);
        return $this->db->resultOne();
    }
    
}
