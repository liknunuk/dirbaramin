<?php
class Model_userAdmin
{
    private $table = "userAdmin";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function uauth($data){
        $userPassword = md5($data['dbPass'] .'***'.$data['dbUser']);
        $sql = "SELECT * FROM " . $this->table . " WHERE userPassword=:userPassword LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('userPassword',$userPassword);
        return $this->db->resultOne();
    }
  
    public function baru($data){
        $userPassword = md5($data['userPassword'] .'***'.$data['userName']);
    /*  [mod] => new 
        [userId] => 
        [namaLengkap] => Kokom 
        [idLembaga] => 1 
        [userName] => jengKokom 
        [userPassword] => Dipayuda987# 
    */
        $sql = "INSERT INTO " . $this->table . " SET userName = :userName , userPassword = :userPassword , namaLengkap = :namaLengkap , idLembaga = :idLembaga , userId = :userId";
        $this->db->query($sql);

        $this->db->bind('userName',$data['userName']);
        $this->db->bind('userPassword',$userPassword);
        $this->db->bind('namaLengkap',$data['namaLengkap']);
        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('userId',NULL);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function ubah($data){
        //  $userPassword = md5($data['userPassword'] .'***'.$data['userName']);

        $sql = "UPDATE " . $this->table . " SET namaLengkap = :namaLengkap , idLembaga = :idLembaga WHERE userId = :userId";
        $this->db->query($sql);

        // $this->db->bind('userName',$data['userName']);
        // $this->db->bind('userPassword',$userPassword);
        $this->db->bind('namaLengkap',$data['namaLengkap']);
        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('userId',$data['userId']);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function hapus($id){
        $sql = "DELETE FROM " . $this->table . " WHERE  userId  = :id ";
        $this->db->query($sql);
        
        $this->db->bind('id',$id);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function tampil($hal = 1){
        $bar = ($hal - 1) * baris;
        $sql = "SELECT userAdmin.*, lembaga.namaLembaga FROM " . $this->table . " , lembaga WHERE lembaga.idLembaga = userAdmin.idLembaga ORDER BY namaLengkap LIMIT {$bar}," . baris;

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function pilih($id){
        
        $sql = "SELECT userAdmin.* , lembaga.namaLembaga FROM " . $this->table . ", lembaga WHERE userId = :id &&lembaga.idLembaga = userAdmin.idLembaga";

        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

    public function gantiSandi($data){
        $userPassword = md5($data['userPassword'] .'***'.$data['userName']);

        $sql = "UPDATE " . $this->table . " SET userName = :userName , userPassword = :userPassword WHERE userId = :userId";
        $this->db->query($sql);

        $iconImg = addslashes(file_get_contents($img['iconImg']['tmp_name']));
        $this->db->bind('userName',$data['userName']);
        $this->db->bind('userPassword',$userPassword);
        $this->db->bind('userId',$data['userId']);

        $this->db->execute();
        return  $this->db->rowCount();
    }

}
