<?php
class Model_direktori
{
    private $table = "direktori";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function baru($data){
        $sql = "INSERT INTO " . $this->table . " SET idLembaga = :idLembaga , idKategori = :idKategori , namaObjek = :namaObjek , alamat = :alamat , kecamatan = :kecamatan , telepon = :telepon , geolokasi = :geolokasi, deskripsi = :deskripsi , website = :website, idDirektori = :idDirektori ";
        $this->db->query($sql);

        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('idKategori',$data['idKategori']);
        $this->db->bind('namaObjek',$data['namaObjek']);
        $this->db->bind('alamat',$data['alamat']);
        $this->db->bind('kecamatan',$data['kecamatan']);
        $this->db->bind('telepon',$data['telepon']);
        $this->db->bind('geolokasi',$data['geolokasi']);
        $this->db->bind('deskripsi',$data['deskripsi']);
        $this->db->bind('website',$data['website']);
        $this->db->bind('idDirektori',NULL);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function ubah($data){
        
        $sql = "UPDATE " . $this->table . " SET idLembaga = :idLembaga , idKategori = :idKategori , namaObjek = :namaObjek , alamat = :alamat , kecamatan = :kecamatan , telepon = :telepon , geolokasi = :geolokasi, deskripsi = :deskripsi , website = :website WHERE idDirektori = :idDirektori ";
        $this->db->query($sql);
        
        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('idKategori',$data['idKategori']);
        $this->db->bind('namaObjek',$data['namaObjek']);
        $this->db->bind('alamat',$data['alamat']);
        $this->db->bind('kecamatan',$data['kecamatan']);
        $this->db->bind('telepon',$data['telepon']);
        $this->db->bind('geolokasi',$data['geolokasi']);
        $this->db->bind('deskripsi',$data['deskripsi']);
        $this->db->bind('website',$data['website']);
        $this->db->bind('idDirektori',$data['idDirektori']);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function hapus($id){
        $sql = "DELETE FROM " . $this->table . " WHERE  idDirektori  = :id ";
        $this->db->query($sql);
        
        $this->db->bind('id',$id);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function tampil($hal = 1){
        $bar = ($hal - 1) * baris;
        $sql = "SELECT direktori.*, lembaga.namaLembaga, kategori.namaKategori FROM " . $this->table . " , lembaga, kategori WHERE lembaga.idLembaga = direktori.idLembaga && kategori.idKategori = direktori.idKategori ORDER BY idDirektori DESC LIMIT {$bar}," . baris;
        // return $this->db->cekQuery($sql);
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function pilih($id){
        
        $sql = "SELECT direktori.*, lembaga.namaLembaga, kategori.namaKategori FROM " . $this->table . " , lembaga, kategori WHERE lembaga.idLembaga = direktori.idLembaga && kategori.idKategori = direktori.idKategori && direktori.idDirektori = :id";

        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

    // request mobile app

    public function daftarDirektori($kat){
        // $sql = "SELECT a.idDirektori,a.namaObjek,  b.imgType, b.iconImg FROM direktori a , iconImages b WHERE idKategori = :idKategori && (b.tableSrc='direktori' && b.tableIdx = a.idDirektori) ORDER BY namaObjek LIMIT 100";
        $sql = "SELECT idDirektori, namaObjek, alamat,kecamatan FROM direktori WHERE idKategori = :idKategori ORDER BY namaObjek LIMIT 100";
        $this->db->query($sql);
        $this->db->bind('idKategori',$kat);
        return $this->db->resultSet();
    }

    public function detil($id){
        $sql = "SELECT direktori.namaObjek, direktori.alamat, direktori.kecamatan , direktori.telepon, direktori.website, direktori.geolokasi, lembaga.namaLembaga, kategori.namaKategori FROM " . $this->table . " , lembaga, kategori WHERE lembaga.idLembaga = direktori.idLembaga && kategori.idKategori = direktori.idKategori && direktori.idDirektori = :id";

        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }
    
    public function deskripsi($id){
        $sql = "SELECT deskripsi FROM direktori WHERE idDirektori = :id ";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }


    public function direktoriYanMum($nama){
        $kat = $this->idxLayananUmum($nama);
        $sql = "SELECT namaObjek, alamat, kecamatan, telepon FROM direktori WHERE idDirektori = :id";
        $this->db->query($sql);
        $this->db->bind('id',$kat['idDirektori']);
        return $this->db->resultSet();
    }

    private function idxLayananUmum($nama){
        $nama = "Layanan Umum - ".$nama;
        $sql = "SELECT idKategori FROM kategori WHERE namaKategori = :nama";
        $this->db->query($sql);
        $this->db->bind('nama',$nama);
        return $this->db->resultOne();
    }

    public function caridir($namadir){
        $dirname = str_replace('-',' ',$namadir);
        $objek = '%'.$dirname.'%';
        $sql = "SELECT direktori.*, lembaga.namaLembaga, kategori.namaKategori FROM " . $this->table . " , lembaga, kategori WHERE namaObjek LIKE :objek && lembaga.idLembaga = direktori.idLembaga && kategori.idKategori = direktori.idKategori ORDER BY idDirektori DESC LIMIT " . baris;
        // return $this->db->cekQuery($sql);
        $this->db->query($sql);
        $this->db->bind('objek',$objek);
        return $this->db->resultSet();
    }

    public function jenjangSekolah(){
        $sql = "SELECT DISTINCT(LEFT(namaObjek,3)) jenjang FROM direktori WHERE idKategori = 8";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function sekolah($jenjang){
        $sql = "SELECT namaObjek nama , alamat , kecamatan FROM direktori WHERE namaObjek like :jenjang ;
        ";
        $this->db->query($sql);
        $this->db->bind('jenjang',$jenjang."%");
        return $this->db->resultSet();
    }

    public function rekap(){
        $sql ="
        SELECT 
        ( SELECT COUNT(*) FROM agenda ) agenda, 
        ( SELECT COUNT(*) FROM berita ) berita , 
        ( SELECT COUNT(*) FROM direktori WHERE idKategori = 3 ) belanja , 
        ( SELECT COUNT(*) FROM direktori WHERE idKategori = 4 ) kuliner , 
        ( SELECT COUNT(*) FROM direktori WHERE idKategori=2 ) wisata,
        ( SELECT COUNT(*) FROM direktori WHERE idKategori BETWEEN 6 and 12 ) yanmum
        ";

        $this->db->query($sql);
        return $this->db->resultOne();
    }


}
