<?php
class Model_lembaga
{
    private $table = "lembaga";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function baru($data){
       
        $sql = "INSERT INTO " . $this->table . " SET namaLembaga = :namaLembaga , idLembaga = :idLembaga";
        $this->db->query($sql);

        $this->db->bind('namaLembaga',$data['namaLembaga']);
        $this->db->bind('idLembaga',NULL);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function ubah($data){

        $sql = "UPDATE " . $this->table . " SET namaLembaga = :namaLembaga WHERE idLembaga = :idLembaga";

        $this->db->query($sql);

        $this->db->bind('namaLembaga',$data['namaLembaga']);
        $this->db->bind('idLembaga',$data['idLembaga']);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function hapus($id){
        $sql = "DELETE FROM " . $this->table . " WHERE  idLembaga  = :id ";
        $this->db->query($sql);
        
        $this->db->bind('id',$id);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function tampil($hal = 1){
        $bar = ($hal - 1) * baris;
        $sql = "SELECT * FROM " . $this->table . " ORDER BY namaLembaga LIMIT {$bar}," . baris;

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function tampilAll(){
        $sql = "SELECT * FROM " . $this->table . " ORDER BY namaLembaga";

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function pilih($id){
        
        $sql = "SELECT * FROM " . $this->table . " WHERE idLembaga = :id";

        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->resultOne();
    }

}
