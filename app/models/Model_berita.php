<?php
class Model_berita
{
    private $table = "berita";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function baru($data){
        
        
        $sql = "INSERT INTO " . $this->table . " SET idLembaga = :idLembaga , judul = :judul , uraian = :uraian , tanggalTerbit = :tanggalTerbit , idBerita  = :idBerita ";
        $this->db->query($sql);

        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('judul',$data['judul']);
        $this->db->bind('uraian',$data['uraian']);
        $this->db->bind('tanggalTerbit',$data['tanggalTerbit']);
        $this->db->bind('idBerita',NULL);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function ubah($data){
        
        $sql = "UPDATE " . $this->table . " SET idLembaga = :idLembaga , judul = :judul , uraian = :uraian , tanggalTerbit = :tanggalTerbit  WHERE idBerita  = :idBerita  ";
        $this->db->query($sql);
        
        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('judul',$data['judul']);
        $this->db->bind('uraian',$data['uraian']);
        $this->db->bind('tanggalTerbit',$data['tanggalTerbit']);
        $this->db->bind('idBerita',$data['idBerita']);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function hapus($id){
        $sql = "DELETE FROM " . $this->table . " WHERE  idBerita  = :id ";
        $this->db->query($sql);
        
        $this->db->bind('id',$id);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function tampil($hal = 1){
        $bar = ($hal - 1) * baris;
        $sql = "SELECT berita.*, lembaga.namaLembaga lembaga FROM " . $this->table . " , lembaga WHERE lembaga.idLembaga = berita.idLembaga ORDER BY tanggalTerbit DESC LIMIT {$bar}," . baris;

        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function pilih($id){
        
        $sql = "SELECT berita.* , lembaga.namaLembaga FROM " . $this->table . ",lembaga WHERE idBerita = :id && lembaga.idLembaga = berita.idLembaga";

        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

    // request mobile app
    public function arsipBerita(){
        $sql = "SELECT idBerita , judul , c.namaLembaga perilis, DATE_FORMAT(tanggalTerbit, '%d-%m-%Y') terbit , b.imgType , b.iconImg FROM berita a, iconImages b , lembaga c WHERE (tableSrc='berita' &&  tableIdx = a.idBerita) && c.idLembaga = a.idLembaga ORDER BY tanggalTerbit DESC LIMIT 50";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function uraianBerita($id){
        $sql = "SELECT uraian FROM berita WHERE idBerita =:id ";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

}
