<?php
class Model_agenda
{
    private $table = "agenda";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function baru($data){

        $sql = "INSERT INTO " . $this->table . " SET idLembaga = :idLembaga , namaAgenda = :namaAgenda , tanggalMulai = :tanggalMulai , tanggalAkhir = :tanggalAkhir , lokasi = :lokasi , deskripsi = :deskripsi , kontakPerson  = :kontakPerson , kontakAddress  = :kontakAddress , idAgenda  = :idAgenda ";
        $this->db->query($sql);
        
        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('namaAgenda',$data['namaAgenda']);
        $this->db->bind('tanggalMulai',$data['tanggalMulai']);
        $this->db->bind('tanggalAkhir',$data['tanggalAkhir']);
        $this->db->bind('lokasi',$data['lokasi']);
        $this->db->bind('deskripsi',$data['deskripsi']);
        $this->db->bind('kontakPerson',$data['kontakPerson']);
        $this->db->bind('kontakAddress',$data['kontakAddress']);
        $this->db->bind('idAgenda',NULL);
        
        $this->db->execute();
        return  $this->db->rowCount();
    }
    
    public function ubah($data){
        
        $sql = "UPDATE " . $this->table . " SET idLembaga = :idLembaga , namaAgenda = :namaAgenda , tanggalMulai = :tanggalMulai , tanggalAkhir = :tanggalAkhir , lokasi = :lokasi , deskripsi = :deskripsi , kontakPerson  = :kontakPerson , kontakAddress  = :kontakAddress WHERE idAgenda  = :idAgenda ";
        $this->db->query($sql);
        
        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('namaAgenda',$data['namaAgenda']);
        $this->db->bind('tanggalMulai',$data['tanggalMulai']);
        $this->db->bind('tanggalAkhir',$data['tanggalAkhir']);
        $this->db->bind('lokasi',$data['lokasi']);
        $this->db->bind('deskripsi',$data['deskripsi']);
        $this->db->bind('kontakPerson',$data['kontakPerson']);
        $this->db->bind('kontakAddress',$data['kontakAddress']);
        $this->db->bind('idAgenda',$data['idAgenda']);
        
        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function hapus($id){
        $sql = "DELETE FROM " . $this->table . " WHERE  idAgenda  = :id ";
        $this->db->query($sql);
        
        $this->db->bind('id',$id);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function tampil($hal = 1){
        $bar = ($hal - 1) * baris;
        $sql = "SELECT * FROM " . $this->table . " ORDER BY tanggalMulai DESC LIMIT {$bar}," . baris;

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function pilih($id){
        
        $sql = "SELECT idAgenda , agenda.idLembaga , namaLembaga kantor , namaAgenda , DATE_FORMAT(tanggalMulai,'%d-%m-%Y') tanggalMulai , DATE_FORMAT(tanggalAkhir,'%d-%m-%Y')tanggalAkhir , lokasi , deskripsi , kontakPerson , kontakAddress FROM " . $this->table . " , lembaga WHERE lembaga.idLembaga = agenda.idLembaga && idAgenda = :id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->resultOne();
    }

    // Request Mobile App
    public function agdThIni(){
        $thIni = date('Y').'%';
        $sql = "SELECT idAgenda , namaAgenda , DATE_FORMAT(tanggalMulai,'%d-%m-%Y') mulai , DATE_FORMAT(tanggalAkhir,'%d-%m-%Y') selesai, lokasi, b.imgType , b.iconImg FROM agenda a , iconImages b WHERE tanggalMulai LIKE :thIni && ( tableSrc='agenda' &&  tableIdx = a.idAgenda )";
        $this->db->query($sql);
        $this->db->bind('thIni',$thIni);
        return $this->db->resultSet();
    }
    
}
