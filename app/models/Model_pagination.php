<?php
class Model_pagination
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function paginate($table,$currentPage=1){
        $sql = "SELECT COUNT(*) baris FROM $table";
        $this->db->query($sql);
        $result = $this->db->resultOne();
        $baris = $result['baris'];
        $data['baris'] = $baris;
        $data['nmhal'] = $currentPage;
        $data['jmhal'] = ceil($baris / baris);
        return $data;
    }

}
