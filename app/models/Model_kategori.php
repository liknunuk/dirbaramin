<?php
class Model_kategori
{
    private $table = "kategori";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function baru($data){
        $sql = "INSERT INTO " . $this->table . " SET idLembaga = :idLembaga , namaKategori = :namaKategori , idKategori = :idKategori ";
        $this->db->query($sql);

        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('namaKategori',$data['namaKategori']);
        $this->db->bind('idKategori',NULL);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function ubah($data){
        $sql = "UPDATE " . $this->table . " SET idLembaga = :idLembaga , namaKategori = :namaKategori  WHERE idKategori = :idKategori ";
        $this->db->query($sql);

        $iconImg = addslashes(file_get_contents($img['iconImg']['tmp_name']));
        $this->db->bind('idLembaga',$data['idLembaga']);
        $this->db->bind('namaKategori',$data['namaKategori']);
        $this->db->bind('idKategori',$data['idKategori']);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function hapus($id){
        $sql = "DELETE FROM " . $this->table . " WHERE  idKategori  = :id ";
        $this->db->query($sql);
        
        $this->db->bind('id',$id);

        $this->db->execute();
        return  $this->db->rowCount();
    }

    public function tampil($hal = 1){
        $bar = ($hal - 1) * baris;
        $sql = "SELECT kategori.* , lembaga.namaLembaga FROM " . $this->table . ",lembaga WHERE kategori.idLembaga = lembaga.idLembaga ORDER BY namaKategori DESC LIMIT {$bar}," . baris;

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function tampilAll(){
        
        $sql = "SELECT * FROM " . $this->table . " ORDER BY namaKategori ";

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function pilih($id){
        
        $sql = "SELECT * FROM " . $this->table . " WHERE idKategori = :id";

        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->resultOne();
    }

    // request mobile app

    public function baku5(){
        $sql = "SELECT idKategori , namaKategori FROM kategori WHERE namaKategori = 'Objek Wisata' || namaKategori = 'Sentra Kuliner' || namaKategori = 'Hotel' || namaKategori = 'Homestay' || namaKategori = 'Tempat Belanja'";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function layananUmum(){
        $sql = "SELECT idKategori , namaKategori FROM kategori WHERE namaKategori LIKE 'LAYANAN UMUM -%'";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // public function idxLayananUmum($nama){
    //     $nama = "Layanan Umum - ".$nama;
    //     $sql = "SELECT idKategori FROM kategori WHERE namaKategori = :nama";
    //     $this->db->query($sql);
    //     $this->db->bind('nama',$nama);
    //     return $this->db->resultOne();
    // }

}
