<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Admin extends Controller
{
  
  public function __construct(){
    if(!isset($_SESSION['admin']) ){
      Alert::setAlert('Login Dulu','Mohon','warning');
      header("Location:" . BASEURL );
    }
  }
  
  public function sitirahat(){
    session_unset();
    session_destroy();
    header("Location:" . BASEURL );
  }
  
  // method default
  public function index()
  {
    $data['title']="Admin Panel - Direktori Banjarnegara";
    $data['rekap'] = $this->model('Model_direktori')->rekap();
    $this->view('template/headersb',$data);
    $this->view('admin/index',$data);
    $this->view('template/footersb');
  }

  public function agenda($pn = 1){
    $data['title']="Admin Panel - Agenda";
    $data['pn'] = $pn;
    $data['agenda'] = $this->model('Model_agenda')->tampil($pn);
    $this->view('template/headersb',$data);
    $this->view('admin/agenda',$data);
    $this->view('template/footersb');
  }

  public function setAgenda(){
    // print_r($_POST);
    // echo "<hr>";
    // print_r($_FILES);
      

    if($_POST['mod'] == 'new'){
      if( $this->model('Model_agenda')->baru($_POST) > 0){
        Alert::setAlert('Berhasil Disimpan','Data Agenda','success');
        header("Location:" . BASEURL . "Admin/agenda/1");
      }else{
        Alert::setAlert('Batal Disimpan','Data Agenda','danger');
        header("Location:" . BASEURL . "Admin/agenda/1");
      }
    }elseif($_POST['mod'] == 'chg'){
      if($this->model('Model_agenda')->ubah($_POST) > 0){
        Alert::setAlert('Berhasil Diubah','Data Agenda','success');
        header("Location:" . BASEURL . "Admin/agenda/1");
      }else{
        Alert::setAlert('Batal Diubah','Data Agenda','danger');
        header("Location:" . BASEURL . "Admin/agenda/1");
      }
    }else{
      header("Location:" . BASEURL . "Admin/agenda/1");
    }
  }

  public function agdinfo($id){
    $data['title']="Admin Panel - Agenda";
    $data['agenda'] = $this->model('Model_agenda')->pilih($id);
    $data['gambar'] = $this->model('Model_images')->showImage('agenda',$id);
    
    $this->view('template/headersb',$data);
    $this->view('admin/info-agenda',$data);
    $this->view('template/footersb');
  }
  
  public function fragenda($id=''){
    $data['title']="Admin Panel - Agenda";
    $data['lembaga'] = $this->model('Model_lembaga')->tampilAll();
    $data['ida'] = $id;
    $data['mod'] = $id == '' ? 'new':'chg';
    if( $id == '' ){
      $data['agenda'] = [
        'idAgenda'=>'','idLembaga'=>'','namaAgenda'=>'','tanggalMulai'=>'','tanggalAkhir'=>'',
        'lokasi'=>'','deskripsi'=>'','kontakPerson'=>'','kontakAddress'=>''
      ];
    }else{
      $data['agenda'] = $this->model('Model_agenda')->pilih($id);
    }
    $this->view('template/headersb',$data);
    $this->view('admin/fragenda',$data);
    $this->view('template/footersb');
  }
  
  public function berita($pn =  1){
    $data['title']="Admin Panel - Berita";
    $data['pn'] = $pn;
    $data['berita'] = $this->model('Model_berita')->tampil($pn);
    $this->view('template/headersb',$data);
    $this->view('admin/berita',$data);
    $this->view('template/footersb');
  }
  
  public function brtinfo($id){
    $data['title']="Admin Panel - Berita";
    $data['berita'] = $this->model('Model_berita')->pilih($id);
    $data['gambar'] = $this->model('Model_images')->showImage('berita',$id);
    $this->view('template/headersb',$data);
    $this->view('admin/info-berita',$data);
    $this->view('template/footersb');
  }
  
  public function frberita($id=''){
    $data['title']="Admin Panel - Berita";
    $data['lembaga'] = $this->model('Model_lembaga')->tampilAll();
    $data['idb'] = $id;
    $data['mod'] = $id == '' ? 'new':'chg';
    if( $id !== '' ){
      $data['berita'] = $this->model('Model_berita')->pilih($id);
    }
    else{
      $data['berita'] = 
      [
        'idBerita' => '', 'idLembaga'=>'', 'judul'=>'','uraian'=>'','tanggalTerbit'=>''
      ];
    }
    $this->view('template/headersb',$data);
    $this->view('admin/frberita',$data);
    $this->view('template/footersb');
  }

  public function setBerita(){
    if($_POST['mod'] == 'new'){
     if($this->model('Model_berita')->baru($_POST) > 0 ){
       Alert::setAlert('Berhasil Disimpan!','Data Berita','success');
       header("Location:".BASEURL."Admin/berita/1");
      }else{
        Alert::setAlert('Gagal Disimpan!','Data Berita','danger');
        header("Location:".BASEURL."Admin/berita/1");
     }
    }else{
      if($this->model('Model_berita')->ubah($_POST) > 0 ){
        Alert::setAlert('Berhasil Disimpan!','Data Berita','success');
        header("Location:".BASEURL."Admin/berita/1");
       }else{
         Alert::setAlert('Gagal Disimpan!','Data Berita','danger');
         header("Location:".BASEURL."Admin/berita/1");
      }
    }
  }


  public function kategori($pn = 1 , $id=''){
    $data['title']="Admin Panel - Kategori";
    $data['pn'] = $pn;
    $data['idk'] = $id;
    $data['lembaga'] = $this->model('Model_lembaga')->tampilAll();
    $data['mod'] = $id == '' ? 'new':'chg';
    $data['ktgr'] = $this->model('Model_kategori')->tampil($pn);
    if($id == ''){
      $data['kategori'] =
      [ 'idKategori'=>'','idLembaga'=>'','namaKategori'=>'' , 'namaLemaga'=>'' ];
    }else{
      $data['kategori'] = $this->model('Model_kategori')->pilih($id);
    }
    $this->view('template/headersb',$data);
    $this->view('admin/kategori',$data);
    $this->view('template/footersb');
  }

  public function setKategori(){
    if($_POST['mod'] == 'new'){
      if($this->model('Model_kategori')->baru($_POST) > 0 ){
        Alert::setAlert('Berhasil disimpan','Data kategori','success');
        header("Location:".BASEURL."Admin/kategori/1");
      }else{
        Alert::setAlert('Gagal disimpan','Data kategori','danger');
        header("Location:".BASEURL."Admin/kategori/1");
      }
    }else{
      if($this->model('Model_kategori')->ubah($_POST) > 0 ){
        Alert::setAlert('Berhasil disimpan','Data kategori','success');
        header("Location:".BASEURL."Admin/kategori/1");
      }else{
        Alert::setAlert('Gagal disimpan','Data kategori','danger');
        header("Location:".BASEURL."Admin/kategori/1");
      }
    }
  }

  public function lembaga($pn = 1 , $id=''){
    $data['title']="Admin Panel - Lembaga";
    $data['pn'] = $pn;
    $data['idl'] = $id;
    $data['mod'] = $id == '' ? 'new':'chg';
    $data['lembaga'] = $this->model('Model_lembaga')->tampil($pn);

    if($id == ''){
      $data['lbg'] = ['idLembaga'=>'','namaLembaga' =>''];
    }else{
      $data['lbg'] = $this->model('Model_lembaga')->pilih($id);
    }

    $this->view('template/headersb',$data);
    $this->view('admin/lembaga',$data);
    $this->view('template/footersb');
  }

  public function setLembaga(){
    
    if($_POST['mod'] == 'new' ){
      if($this->model('Model_lembaga')->baru($_POST) > 0 ){
        Alert::setAlert('Berhasil Disimpan' , 'Data Lembaga' , 'success');
        header("Location:" . BASEURL . "Admin/lembaga/1");
      }else{
        Alert::setAlert('Gagal Disimpan' , 'Data Lembaga' , 'danger');
        header("Location:" . BASEURL . "Admin/lembaga/1");
      }
    }elseif($_POST['mod'] == 'chg' ){
      if($this->model('Model_lembaga')->ubah($_POST) > 0 ){
        Alert::setAlert('Berhasil Diubah' , 'Data Lembaga' , 'success');
        header("Location:" . BASEURL . "Admin/lembaga/1");
      }else{
        Alert::setAlert('Gagal Diubah' , 'Data Lembaga' , 'danger');
        header("Location:" . BASEURL . "Admin/lembaga/1");
      }
    }else{
      header("Location:" . BASEURL . "Admin/lembaga/1");
    }
  }
  
  public function users($pn =  1){
    $data['title']="Admin Panel - Pengelola";
    $data['pn'] = $pn;
    $data['user'] = $this->model('Model_userAdmin')->tampil($pn);
    $this->view('template/headersb',$data);
    $this->view('admin/users',$data);
    $this->view('template/footersb');
  }

  public function fruser($id=''){
    $data['title']="Admin Panel - User";
    $data['idu'] = $id;
    $data['mod'] = $id == '' ? 'new':'chg';
    $data['lembaga'] = $this->model('Model_lembaga')->tampilAll();
    if( $id == '' ){
      $data['user'] = [
        'userId' =>'' , 'userName' =>'' , 'userPassword' =>'' , 'namaLengkap' =>'' , 'idLembaga' =>''
      ];
    }else{
      $data['user'] = $this->model('Model_userAdmin')->pilih($id);
    }
    $this->view('template/headersb',$data);
    $this->view('admin/fruser',$data);
    $this->view('template/footersb');
  }

  public function setUser(){
    if($_POST['mod'] == 'new'){
      
      if($this->model('Model_userAdmin')->baru($_POST) > 0 ){
        Alert::setAlert('Berhasil Disimpan','Data User','success');
        header("Location:".BASEURL."Admin/users/1");
      }else{
          Alert::setAlert('Gagal Disimpan','Data User','danger');
          header("Location:".BASEURL."Admin/users/1");
      }

    }else{

      if($this->model('Model_userAdmin')->ubah($_POST) > 0 ){
        Alert::setAlert('Berhasil Diubah','Data User','success');
        header("Location:".BASEURL."Admin/users/1");
      }else{
          Alert::setAlert('Gagal Diubah','Data User','danger');
          header("Location:".BASEURL."Admin/users/1");
      } 

    }
  }

  public function usrfired($id,$confirm='false'){
    $data['title']="Admin Panel - User";
    
    if( $confirm == 'true' ){
      if($this->model('Model_userAdmin')->hapus($id) > 0 ){
        Alert::setAlert('Berhasil Dihapus','Data User Admin','success');
        header("Location:".BASEURL."Admin/users/1");
      }else{
        Alert::setAlert('Gagal Dihapus','Data User Admin','danger');
        header("Location:".BASEURL."Admin/users/1");
      }
    }else{
      $data['user'] = $this->model('Model_userAdmin')->pilih($id);
      $this->view('template/headersb',$data);
      $this->view('admin/user-pensiun',$data);
      $this->view('template/footersb');
    }
  }

  public function usrcreds($id){
    $data['title']="Admin Panel - User";
    $data['id'] = $id;
    $data['user'] = $this->model('Model_userAdmin')->pilih($id);
    $this->view('template/headersb',$data);
    $this->view('admin/user-cred',$data);
    $this->view('template/footersb');
  }

  public function setusercred(){
    $data['user']=$this->model('Model_userAdmin')->pilih($_POST['userId']);
    // echo "<pre>";
    // print_r($_POST);
    $sendAuth = md5($_POST['oldPassword'].'***'.$_POST['userName']);
    // echo "Password dikirim   : " . $sendAuth .'<br>';
    // echo "Password tersimpan : " . $data['user']['userPassword'].'<br>';
    if( $sendAuth == $data['user']['userPassword']){
      if($this->model('Model_userAdmin')->gantiSandi($_POST) > 0 ){
        Alert::setAlert('Berhasil diubah','Password user','success');
        header("Location:".BASEURL."Admin/users/1");  
      }else{
        Alert::setAlert('Gagal diubah','Password user','danger');
        header("Location:".BASEURL."Admin/users/1");
      }
    }else{
      Alert::setAlert('Ditolak','Password Lama','danger');
      header("Location:".BASEURL."Admin/usrcreds/".$_POST['userId']);
    }
    // echo "</pre>";
  }
  
  public function direktori($pn=1){
    $data['title'] = 'Admin Panel - Direktori';
    $data['pn'] = $pn;
    $data['direktori'] = $this->model('Model_direktori')->tampil($pn);
    // $data['halaman'] = $this->model('Model_pagination')->paginate('direktori',$pn);
    // echo $data['direktori'];
    $this->view('template/headersb',$data);
    $this->view('admin/direktori',$data);
    $this->view('template/footersb');
  }
  
  public function dirinfo($id){
    $data['title']="Admin Panel - Direktori";
    $data['gambar'] = $this->model('Model_images')->showImage('direktori',$id);
    $data['direktori'] = $this->model('Model_direktori')->pilih($id);
    $this->view('template/headersb',$data);
    $this->view('admin/info-direktori',$data);
    $this->view('template/footersb');
  }
  
  public function frdirektori($id=''){
    if($id == '' ){
      $data['direktori'] = [
        'idDirektori'=>'','idKategori'=>'','idLembaga'=>'','namaObjek'=>'','alamat'=>'','kecamatan'=>'','telepon'=>'','email'=>'','geolokasi'=>'','deskripsi'=>'','website'=>''
      ];
    }else{
      $data['direktori'] = $this->model('Model_direktori')->pilih($id);
    }
    $data['title']="Admin Panel - Direktori";
    $data['id'] = $id;
    $data['mod'] = $id == '' ? 'new':'chg';
    $data['lembaga'] = $this->model('Model_lembaga')->tampilAll();
    $data['kategori'] = $this->model('Model_kategori')->tampilAll();
    $this->view('template/headersb',$data);
    $this->view('admin/frdirektori',$data);
    $this->view('template/footersb');
  }

  public function setDirektori(){
    if($_POST['mod'] == 'new'){
      if($this->model('Model_direktori')->baru($_POST) > 0 ){
        Alert::setAlert('Berhasil disimpan','Data direktori','success');
        header('Location:'.BASEURL.'Admin/direktori/1');
      }else{
        Alert::setAlert('Gagal disimpan','Data direktori','danger');
        header('Location:'.BASEURL.'Admin/direktori/1');
      }
    }else{
      if($this->model('Model_direktori')->ubah($_POST) > 0 ){
        Alert::setAlert('Berhasil disimpan','Data direktori','success');
        header('Location:'.BASEURL.'Admin/direktori/1');
      }else{
        Alert::setAlert('Gagal disimpan','Data direktori','danger');
        header('Location:'.BASEURL.'Admin/direktori/1');
      }
    }
  }

  public function setIcon($tab,$idx,$rpg,$mod='new'){
    $data['title'] = 'Set Icon';
    $data['form'] = ['tabel'=>$tab , 'nomor' =>$idx ];
    $data['prev'] = $rpg;
    $data['mod'] = $mod;
    
    $this->view('template/headersb',$data);
    $this->view('admin/fricon',$data);
    $this->view('template/footersb');
  }

  public function saveIcon(){

    // Encode the image string data into base64 
    $image = base64_encode( file_get_contents( $_FILES['iconImg']['tmp_name'] ) ); 
    $imgData = [
      'tabel'=>$_POST['tableSrc'],
      'nomor'=>$_POST['tableIdx'],
      'imgtp'=>$_FILES['iconImg']['type'],
      'image'=>$image
    ];
    
    if($_POST['mod'] == 'new' ){
      if($this->model('Model_images')->setImage($imgData) > 0 ){
        Alert::setAlert('Berhasil Disimpan' , 'Gambar Ikon' , 'success');
        header("Location:" . BASEURL . 'Admin/' . $_POST['returnPage'] .'/'.$_POST['tableIdx']); 
      }
    }else{
        if($this->model('Model_images')->chgImage($imgData) > 0 ){
          Alert::setAlert('Berhasil Disimpan' , 'Gambar Ikon' , 'success');
          header("Location:" . BASEURL . 'Admin/' . $_POST['returnPage'] .'/'.$_POST['tableIdx']); 
        }
    }
  }

}