<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Mobile extends Controller
{

    public function __construct(){
      $this->view('template/cors');
    }

    public function index(){
        $data['title']='Mobile Request';
        $this->view('template/header',$data);
        echo "<h1>Directory Banjarnegara</h1>";
        $this->view('template/footer');
    }

    public function beranda(){
      $kategori = $this->model('Model_kategori')->baku5();
      echo json_encode($kategori,JSON_PRETTY_PRINT);
    }

    public function daftarAgenda(){
      $agenda = $this->model('Model_agenda')->agdThIni();
      echo json_encode($agenda,JSON_PRETTY_PRINT);
    }
    
    public function detilAgenda($id){
        $agenda = $this->model('Model_agenda')->pilih($id);
        echo json_encode($agenda,JSON_PRETTY_PRINT);
    }
    
    public function arsipBerita(){
        $berita = $this->model('Model_berita')->arsipBerita();
        echo json_encode($berita,JSON_PRETTY_PRINT);
    }
    
    public function detilBerita($id){
        $berita = $this->model('Model_berita')->pilih($id);
        echo json_encode($berita,JSON_PRETTY_PRINT);
    }
    
    public function uraianBerita($id){
        $berita = $this->model('Model_berita')->uraianBerita($id);
        echo $berita['uraian'];
    }

    public function kategorine($idKat){
        $kategori = $this->model('Model_kategori')->pilih($idKat);
        echo $kategori['namaKategori'];
    }

    public function directoryList($kat){
        $objekDirektori = $this->model('Model_direktori')->daftarDirektori($kat);
        echo json_encode($objekDirektori,JSON_PRETTY_PRINT);
    }
    
    public function detilDirektori($id){
        // $objekDirektori = $this->model('Model_direktori')->pilih($id);
        $objekDirektori = $this->model('Model_direktori')->detil($id);
        echo json_encode($objekDirektori,JSON_PRETTY_PRINT);
    }

    public function uraiDirektori($id){
        // $objekDirektori = $this->model('Model_direktori')->pilih($id);
        $objekDirektori = $this->model('Model_direktori')->deskripsi($id);
        echo $objekDirektori['deskripsi'];
    }
    
    public function jenisPublicServis(){
        $yanum = $this->model('Model_kategori')->layananUmum();        
        echo json_encode($yanum,JSON_PRETTY_PRINT);
    }

    public function dirSrc($namadir){
        $dirs=$this->model('Model_direktori')->caridir($namadir);
        echo json_encode($dirs,JSON_PRETTY_PRINT);
    }

    public function flagIcon(){
        echo "
        <img src='".BASEURL."img/flag-icon.png'>
        ";
    }

    public function jenjangSekolah(){
        $jenjang = $this->model('Model_direktori')->jenjangSekolah();
        echo json_encode($jenjang,JSON_PRETTY_PRINT);
    }

    public function sekolah($jenjang){
        $sekolah = $this->model('Model_direktori')->sekolah($jenjang);
        echo json_encode($sekolah, JSON_PRETTY_PRINT);
    }

}
