<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $data['title']  = 'Direktori Banjarnegara';
    $this->view('template/header',$data);
    $this->view('home/index');
    $this->view('template/footer');
  }

  public function auth(){
    $data['user'] = $this->model('Model_userAdmin')->uauth($_POST);
    // print_r($data['user']);
    if($data['user']==NULL || $data['user'] == "" ){
      Alert::setAlert('tidak ditemukan' , 'Data user' , 'warning');
      header('Location:' . BASEURL );
    }else{
      $_SESSION['admin'] = $data['user']['namaLengkap'];
      header('Location:' . BASEURL ."Admin");
    }
  }


  public function Deletion($table){
    // echo "Data {$table} nomor ".$_POST['id']." Telah Dihapus";
    // echo "Data {$table} nomor {$id} Telah Dihapus";
    switch($table){
      case 'agenda'     : $this->hapusAgenda($_POST);     break;
      case 'berita'     : $this->hapusBerita($_POST);     break;
      case 'direktori'  : $this->hapusDirektori($_POST);  break;
      case 'kategori'   : $this->hapusKategori($_POST);   break;
      case 'lembaga'    : $this->hapusLembaga($_POST);    break;
    }
  }
  
  private function hapusAgenda($data){
    if( $this->model('Model_agenda')->hapus($data['id']) > 0 ){
      Alert::setAlert('Telah dihapus','Data agenda', 'primary');
    }else{
      Alert::setAlert('Gagal dihapus','Data agenda', 'warning');
    }
  }

  private function hapusBerita($data){
    if( $this->model('Model_berita')->hapus($data['id']) > 0 ){
      Alert::setAlert('Telah dihapus','Data berita', 'primary');
    }else{
      Alert::setAlert('Gagal dihapus','Data berita', 'warning');
    }
  }

  private function hapusDirektori($data){
    if( $this->model('Model_direktori')->hapus($data['id']) > 0 ){
      Alert::setAlert('Telah dihapus','Data direktori', 'primary');
    }else{
      Alert::setAlert('Gagal dihapus','Data direktori', 'warning');
    }
  }

  private function hapusKategori($data){
    if( $this->model('Model_kategori')->hapus($data['id']) > 0 ){
      Alert::setAlert('Telah dihapus','Data kategori', 'primary');
    }else{
      Alert::setAlert('Gagal dihapus','Data kategori', 'warning');
    }
  }

  private function hapusLembaga($data){
    if( $this->model('Model_lembaga')->hapus($data['id']) > 0 ){
      Alert::setAlert('Telah dihapus','Data lembaga', 'primary');
    }else{
      Alert::setAlert('Gagal dihapus','Data lembaga', 'warning');
    }
  }
}
