<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Image extends Controller
{
  // method default
  public function index()
  {
    $this->view('images/index');
  }

  public function display($table,$id){
    $img = $this->model('Model_images')->showImage($table,$id);
    echo '<img src="data:'.$img['imgType'].';base64,' . $img['iconImg'] . '" />';
  }
}
