<div id="loginBoxWrapper">
    <div id="loginBoxBanner">
        <p>Direktori Banjarnegara</p>
    </div>
    <div id="loginBoxForm">
        <?php Alert::sankil(); ?>
        <form action="<?=BASEURL;?>Home/auth" method="post">
            <div class="form-group">
                <label for="dbUser">Username</label>
                <input type="text" class="form-control" name="dbUser" id="dbUser" placeHolder="Username">
            </div>
            <div class="form-group">
                <label for="dbPass">Password</label>
                <input type="password" class="form-control" name="dbPass" id="dbPass" placeHolder="Password">
            </div>
            <div class="form-group d-flex justify-content-end">
                <input type="submit" class="btn btn-success" id="dbSubmit" value="Masuk">
            </div>
        </form>
    </div>
</div>
<p class="text-light">dinkominfo-banjarnegara</p>

<?php $this->view('template/bs4js'); ?>
