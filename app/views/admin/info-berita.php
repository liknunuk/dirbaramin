<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-secondary text-light text-center">
                <p class="h2"><?=$data['berita']['judul'];?></p>
            </div>
            <div class="card-body indent-5 #uraian">
                <?=$data['berita']['uraian'];?>
            </div>
            <div class="card-body">
            <?php
                if(empty($data['gambar'])){
                    echo "<a class='btn btn-primary' href='".BASEURL."Admin/setIcon/berita/{$data[berita][idBerita]}/brtInfo'>Set Icon</a>";
                }else{
                    echo '<img width="90px" height="90px" src="data:'.$data['gambar']['imgType'].';base64,' . $data['gambar']['iconImg'] . '" />';
                    echo "<br>";
                    echo "<a class='btn btn-primary' href='".BASEURL."Admin/setIcon/berita/{$data['berita']['idBerita']}/brtInfo/chg'>Ganti Icon</a>";
                }
                ?>
            </div>
        </div>
    </div>
</div>TRMS 