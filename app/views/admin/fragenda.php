<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
        <div class="card-header bg-secondary text-light text-center">
                <p class="h2">Agenda Kegiatan Baru</p>
                
            </div>
            <div class="card-body">
                <form action="<?=BASEURL;?>Admin/setAgenda" enctype="multipart/form-data" method="post">
                <input type="hidden" name="mod" value="<?=$data['mod'];?>">
                    <div class="form-group row">
                        <label for="idAgenda" class="col-md-3">Nomor ID</label>
                        <div class="col-md-9">
                            <input type="text" name="idAgenda" id="idAgenda" class="form-control" readonly value="<?=$data['agenda']['idAgenda'];?>" >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="idLembaga" class="col-md-3">Perilis</label>
                        <div class="col-md-9">
                            <select name="idLembaga" id="idLembaga" class="form-control">
                                <option value="">Pilih Lembaga Perilis</option>
                                <?php foreach($data['lembaga'] AS $lembaga):?>
                                <option value="<?=$lembaga['idLembaga'];?>"
                                <?php if($lembaga['idLembaga'] == $data['agenda']['idLembaga']) echo "selected ";?> 
                                ><?=$lembaga['namaLembaga'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="namaAgenda" class="col-md-3">Nama Agenda Kegiatan</label>
                        <div class="col-md-9">
                            <input type="text" name="namaAgenda" id="namaAgenda" class="form-control" value="<?=$data['agenda']['namaAgenda'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tanggalMulai" class="col-md-3">Tanggal Penyelenggaraan</label>
                        <div class="col-md-4">
                            <input type="date" name="tanggalMulai" id="tanggalMulai" class="form-control" value="<?=$data['agenda']['tanggalMulai'];?>">
                        </div>
                        <div class="col-md-1">sampai</div>
                        <div class="col-md-4">
                            <input type="date" name="tanggalAkhir" id="tanggalAkhir" class="form-control" value="<?=$data['agenda']['tanggalAkhir'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lokasi" class="col-md-3">Lokasi Kegiatan</label>
                        <div class="col-md-9">
                            <input type="text" name="lokasi" id="lokasi" class="form-control" value="<?=$data['agenda']['lokasi'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="deskripsi" class="col-md-3">Deskripsi</label>
                        <div class="col-md-9">
                            <textarea rows='12' style="resize:none;" name="deskripsi" id="deskripsi" class="form-control deskripsi"><?=$data['agenda']['deskripsi'];?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="kontakPerson" class="col-md-3">Contact Person</label>
                        <div class="col-md-9">
                            <input type="text" name="kontakPerson" id="kontakPerson" class="form-control"  value="<?=$data['agenda']['kontakPerson'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="kontakAddress" class="col-md-3">Alamat Kontak</label>
                        <div class="col-md-9">
                            <input type="text" name="kontakAddress" id="kontakAddress" class="form-control"  value="<?=$data['agenda']['kontakAddress'];?>">
                        </div>
                    </div>

                    <!--div class="form-group row">
                        <label for="iconImg" class="col-md-3">Ikon Kegiatan</label>
                        <div class="col-md-9">
                            <input type="file" name="iconImg" id="iconImg" class="form-control" accept="image/png" >
                        </div>
                    </div-->

                    <div class="form-group row">
                        <label for="agd_" class="col-md-3 bg-warning">Cek Data</label>
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>