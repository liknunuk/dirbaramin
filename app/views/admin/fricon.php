<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <P class="h1 text-center bg-secondary text-light py-2">Form Set Gambar Icon</P>
            <form action="<?=BASEURL;?>Admin/saveIcon" method="post" enctype="multipart/form-data">

                <input type="hidden" name="returnPage" value="<?=$data['prev'];?>">                
                <input type="hidden" name="mod" value="<?=$data['mod'];?>">

                <div class="form-group row">
                    <label for="tableSrc" class="col-md-4">Objek Ikon</label>
                    <div class="col-md-8">
                        <input type="text" name="tableSrc" id="tableSrc" class="form-control" value="<?=$data['form']['tabel'];?>" readonly >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tableIdx" class="col-md-4">Indeks Entry</label>
                    <div class="col-md-8">
                        <input type="text" name="tableIdx" id="tableIdx" class="form-control" value="<?=$data['form']['nomor'];?>" readonly >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="iconImg" class="col-md-4">Berkas Gambar</label>
                    <div class="col-md-8">
                        <input type="file" name="iconImg" id="iconImg" accept="image/png">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="xxx" class="col-md-4"></label>
                    <div class="col-md-8 text-right">
                        <button type="submit" class="btn btn-primary">Simpan Gambar</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>