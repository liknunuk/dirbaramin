<?php

$initpass = ['Banjar123#','Gilar456#','Negara789#','Dipayuda987#','Kolopaking654#','Dawetayu321#'];
$idpasswd = mt_rand(0,5);

?>
<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary">
                <p class="h2">USER BARU</p>
            </div>
            <div class="card-body">
                <form action="<?=BASEURL;?>Admin/setuser" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="mod" value="<?=$data['mod'];?>">
                    <div class="form-group row">
                        <label for="userId" class="col-md-3">ID User</label>
                        <div class="col-md-9">
                            <input type="text" name="userId" id="userId" class="form-control" readonly value="<?=$data['user']['userId'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="namaLengkap" class="col-md-3">Nama Lengkap</label>
                        <div class="col-md-9">
                            <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" value="<?=$data['user']['namaLengkap'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="idLembaga" class="col-md-3">Lembaga</label>
                        <div class="col-md-9">
                            <select name="idLembaga" id="idLembaga" class="form-control">
                                <?php $i=0; foreach($data['lembaga'] AS $lbg): ?>
                                <option <?php if($data['user']['userId'] == $lbg['idLembaga'] ){ echo "selected"; } ?> value="<?=$lbg['idLembaga'];?>"><?=$lbg['namaLembaga'];?></option>
                                <?php $i++; endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <?php if($data['mod'] == 'new'): ?>
                    <div class="form-group row">
                        <label for="userName" class="col-md-3">Username</label>
                        <div class="col-md-9">
                            <input type="text" name="userName" id="userName" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="userPassword" class="col-md-3">Password Awal</label>
                        <div class="col-md-9">
                            <input type="text" name="userPassword" id="userPassword" class="form-control" value="<?=$initpass[$idpasswd];?>">
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="form-group row">
                        <label for="usr_submit" class="col-md-3 bg-warning">Periksa Data!</label>
                        <div class="col-md-9 text-right">
                            <input type="submit" name="usr_submit" id="usr_submit" class="btn btn-primary" value="Simpan">
                        </div>
                    </div>

                </form>
            </div>        
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>