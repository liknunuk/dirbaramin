<div class="container-fluid">
  <div class="row" id="admin-main">
    
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
  let items = [
      {'judul':'Kalender Kegiatan' , 'image':'beranda-agenda.png','alternate':'Kalender','entri':'<?=$data['rekap']['agenda'];?>'},
      {'judul':'Pusat Perbelanjaan' , 'image':'beranda-belanja.png','alternate':'Belanja','entri':'<?=$data['rekap']['belanja'];?>'},
      {'judul':'Koleksi Berita' , 'image':'beranda-berita.png','alternate':'Berita','entri':'<?=$data['rekap']['berita'];?>'},
      {'judul':'Pusat Kuliner' , 'image':'beranda-kuliner.png','alternate':'Kuliner','entri':'<?=$data['rekap']['kuliner'];?>'},
      {'judul':'Pelayanan Umum' , 'image':'beranda-layananumum.png','alternate':'Public Service','entri':'<?=$data['rekap']['yanmum'];?>'},
      {'judul':'Tempat Wisata' , 'image':'beranda-wisata.png','alternate':'Objek Wisata','entri':'<?=$data['rekap']['wisata'];?>'}
  ];

  $(document).ready( function(){
      $("#admin-main div").remove();
      $.each( items , function(i,data){
        $("#admin-main").append(`
            <div class="col-sm-4 py-3 px-3">
                <div class="card" style="width: 18rem;">
                    <img src="<?=BASEURL;?>img/${data.image}" class="card-img-top main-img" alt="${data.alternate}">
                    <div class="card-body">
                        <h5 class="card-title bg-primary text-light text-center py-2">${data.judul}</h5>
                        <p class="card-text"> ${data.entri} entries.</p>
                    </div>
                </div>
            </div>
        `);
      })
  })
</script>