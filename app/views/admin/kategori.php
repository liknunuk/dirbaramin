<?php
// $kategori = [
//     ['id'=>'0','name'=>'Tidak Berkategori','penjab'=>'Dinkominfo'],
//     ['id'=>'1','name'=>'Wisata','penjab'=>'Disparbud'],
//     ['id'=>'2','name'=>'Belanja','penjab'=>'Indagkop'],
//     ['id'=>'3','name'=>'Kuliner','penjab'=>'Indagkop'],
//     ['id'=>'4','name'=>'Layanan Umum','penjab'=>'Dinkominfo'],
//     ['id'=>'5','name'=>'Layanan Umum - Kesehatan','penjab'=>'Dinkes'],
//     ['id'=>'6','name'=>'Layanan Umum - Keamanan','penjab'=>'Kesbangpol'],
//     ['id'=>'7','name'=>'Layanan Umum - Pendidikan','penjab'=>'Dindikpora']
// ];
// $katId=$data['idk'];
// if( $katId != ''){ 
//     $ktg = $kategori[$katId];
// }else{
//     $ktg = $kategori[0];
// }
?>
<div class="row mb-1">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary">
                <p class="h2">KATEGORI BARU</p>
            </div>
        </div>
        <div class="card-body">
            <form class="form-inline" action="<?=BASEURL;?>Admin/setKategori" method="POST">
                <input type="hidden" name="mod" value="<?=$data['mod'];?>">
                <label class="sr-only" for="idKategori">ID</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                    <div class="input-group-text">ID</div>
                    </div>
                    <input type="text" class="form-control" id="idKategori" name="idKategori" placeholder="ID Kategori" readonly value="<?=$data['kategori']['idKategori'];?>">
                </div>
                
                <label class="sr-only" for="namaKategori">NAMA</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                    <div class="input-group-text">Kat</div>
                    </div>
                    <input type="text" class="form-control" id="namaKategori" name="namaKategori" placeholder="Nama Kategori" value="<?=$data['kategori']['namaKategori'];?>" >
                </div>
                
                <label class="sr-only" for="idLembaga">Lembaga Penjab</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                    <div class="input-group-text">Penjab</div>
                    </div>
                    <select class="form-control" id="idLembaga" name="idLembaga">
                        <?php foreach($data['lembaga'] AS $lembaga ): ?>
                        <option value="<?=$lembaga['idLembaga'];?>"
                        <?php if($data['lembaga']['idLembaga'] == $lembaga['idLembaga']) echo "Selected "; ?>
                        >
                        <?=$lembaga['namaLembaga'];?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                
                <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </form>
        </div>
    </div>
</div>
<div class="row mb-1">

    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary">
                <p class="h2">DAFTAR KATEGORI</p>
                <?php Alert::sankil(); ?>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-sm table-striped">
                <thead class="bg-dark text-light text-center">
                    <tr>
                        <th>ID</th>
                        <th>Kategori</th>
                        <th>Penanggung Jawab</th>
                        <th>
                            <img src="<?=BASEURL;?>img/admin-control.png" alt="Kontrol" width="20px">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['ktgr'] as $kat): ?>
                        <tr>
                            <td><?=$kat['idKategori'];?></td>
                            <td><?=$kat['namaKategori'];?></td>
                            <td><?=$kat['namaLembaga'];?></td>
                            <td class="text-center">
                                <a href="<?=BASEURL."Admin/kategori/{$data['pn']}/{$kat['idKategori']}";?>" >
                                    <img src="<?=BASEURL;?>img/admin-edit.png" alt="info" style="width:24px; height:24px;">
                                </a> |
                                <a href="#">
                                    <img src="<?=BASEURL;?>img/admin-trash.png" alt="trash" style="width:24px; height:24px;" class='trash' id='<?=$kat['idKategori'];?>'>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready( function(){
    $('.trash').click(function(){
        let id = $(this).prop('id');
        let tenan = confirm('Data Akan Dihapus');
        if( tenan == true ){
            $.post('<?=BASEURL;?>Home/Deletion/kategori/',{
                id: id
            },function(resp){
                //alert(resp);  
                location.reload();
            })
        }
    })
})
</script>