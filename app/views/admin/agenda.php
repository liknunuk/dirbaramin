
<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary text-light text-center">
                <p class="h2">Kalender Kegiatan</p>
                <?php Alert::sankil(); ?>
            </div>
            <div class="card-body table-responsive">
            <a href="<?=BASEURL;?>Admin/fragenda" class="btn btn-primary">Agenda Baru</a>
                <table class="table table-sm table-bordered table-striped">
                    <thead class='bg-dark text-light text-center'>
                        <tr>
                            <th>Tanggal</th>
                            <th>Agenda</th>
                            <th>Lokasi</th>
                            <th>Nama Kontak</th>
                            <th>Nomor Kontak</th>
                            <th>
                                <img src="<?=BASEURL;?>img/admin-control.png" alt="Kontrol" width="20px">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['agenda'] AS $agd): ?>
                            <tr>
                                <td><?=$agd['tanggalMulai'];?></td>
                                <td><?=$agd['namaAgenda'];?></td>
                                <td><?=$agd['lokasi'];?></td>
                                <td><?=$agd['kontakPerson'];?></td>
                                <td><?=$agd['kontakAddress'];?></td>
                                <td align='center'>
                                    <a href="<?=BASEURL;?>Admin/agdinfo/<?=$agd['idAgenda'];?>">
                                        <img src="<?=BASEURL;?>img/admin-info.png" alt="info" style="width:24px; height:24px;">
                                    </a>
                                    <a href="<?=BASEURL;?>Admin/fragenda/<?=$agd['idAgenda'];?>"> |
                                        <img src="<?=BASEURL;?>img/admin-edit.png" alt="edit" style="width:24px; height:24px;">
                                    </a> |
                                    <a href="javascript:void(0)">
                                        <img src="<?=BASEURL;?>img/admin-trash.png" alt="trash" style="width:24px; height:24px;" class='trash' id='<?=$agd['idAgenda'];?>'>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>     
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Admin/agenda/1">1</a></li>
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Admin/agenda/2">2</a></li>
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Admin/agenda/3">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready(function(){
    let pn = <?=$data['pn'];?>;
    let id = pn-1;
    $('li.page-item').eq(pn).addClass('active');

    $('.trash').click(function(){
        let id = $(this).prop('id');
        let tenan = confirm('Data Akan Dihapus');
        if( tenan == true ){
            $.post('<?=BASEURL;?>Home/Deletion/agenda/',{
                id: id
            },function(resp){
                //alert(resp);  
                location.reload();
            })
        }
    })
})
</script>