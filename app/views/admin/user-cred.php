<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary">
                <p class="h2">UBAH PASSWORD</p>
                <?php Alert::sankil(); ?>
            </div>
            <div class="card-body">
                <form action="<?=BASEURL;?>Admin/setusercred" enctype="multipart/form-data" method="post">
                    
                    <div class="form-group row">
                        <label for="userId" class="col-md-3">ID User</label>
                        <div class="col-md-9">
                            <input type="text" name="userId" id="userId" class="form-control" readonly value="<?=$data['user']['userId'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="userName" class="col-md-3">Username</label>
                        <div class="col-md-9">
                            <input type="text" name="userName" id="userName" class="form-control" value=<?=$data['user']['userName'];?>>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="oldPassword" class="col-md-3">Password Semula</label>
                        <div class="col-md-9">
                            <input type="password" name="oldPassword" id="oldPassword" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="userPassword" class="col-md-3">Password Baru</label>
                        <div class="col-md-9">
                            <input type="password" name="userPassword" id="userPassword" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="usr_pass2" class="col-md-3">Konirmasi Password Baru</label>
                        <div class="col-md-9">
                            <input type="password"  id="usr_pass2" class="form-control">
                            <div id="cekPass"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="usr_submit" class="col-md-3 bg-warning">Periksa Data!</label>
                        <div class="col-md-9 text-right">
                            <input type="submit" name="usr_submit" id="usr_submit" class="btn btn-primary" value="Simpan">
                        </div>
                    </div>

                </form>
            </div>        
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
$('#usr_pass2').keyup( function(){
    var p1 = $('#userPassword').val();
    var p2 = $(this).val();
    if( p2.length == p1.length ){
        if(p2 == p1){
            $('#usr_pass2').prop('disable',true);
        }else{
            $('#userPassword').val('');
            $('#userPassword').focus();
            $('#usr_pass2').val('');
            $('#cekPass').html('Password Tidak Identik');
        }
    }
})
</script>