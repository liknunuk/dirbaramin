<?php

$agenda = [
    ['kiri'=>'Id Agenda','kanan'=>$data['agenda']['idAgenda']],
    ['kiri'=>'Penjab','kanan'=>$data['agenda']['kantor']],
    ['kiri'=>'Nama Kegiatan','kanan'=>$data['agenda']['namaAgenda']],
    ['kiri'=>'Mulai','kanan'=>$data['agenda']['tanggalMulai']],
    ['kiri'=>'Selesai','kanan'=>$data['agenda']['tanggalAkhir']],
    ['kiri'=>'Lokasi','kanan'=>$data['agenda']['lokasi']],
    ['kiri'=>'Deskripsi','kanan'=>$data['agenda']['deskripsi']],
    ['kiri'=>'Informasi','kanan'=>$data['agenda']['kontakPerson'] .", ".$data['agenda']['kontakAddress']],
    ['kiri'=>'Icon','kanan'=>$data['gambar']['iconImg']]
];

$idAgenda = $agenda[0]['kanan'];
?>
<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary text-light text-center">
                <p class="h2">Informasi Kegiatan</p>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-striped table-sm">
                    <tbody>
                        <?php $i=1; foreach($agenda AS $agd): ?>
                        <?php if($i<9): ?>
                            <tr>
                                <th><?=$agd['kiri'];?></th>
                                <td><?=$agd['kanan'];?></td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <th><?=$agd['kiri'];?></th>
                                <td>
                                    <?php
                                    if(empty($agd['kanan'])){
                                        echo "<a class='btn btn-primary' href='".BASEURL."Admin/setIcon/agenda/${idAgenda}/agdInfo'>Set Icon</a>";
                                    }else{
                                        echo '<img src="data:'.$data['gambar']['imgType'].';base64,' . $data['gambar']['iconImg'] . '" width="60px"/>';
                                        echo "<br>";
                                        echo "<a class='btn btn-primary' href='".BASEURL."Admin/setIcon/agenda/${idAgenda}/agdInfo/chg'>Ganti Icon</a>";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php $i++; endforeach; ?>
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
