<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
        <div class="card-header bg-secondary text-light text-center">
                <p class="h2">Direktori Baru</p>
            </div>
            <div class="card-body">
                <form action="<?=BASEURL;?>Admin/setDirektori" enctype="multipart-form-data" method="post">
                    <input type="hidden" name="mod" value="<?=$data['mod'];?>">

<!-- idDirektori,idKategori,idLembaga,namaObjek,alamat,kecamatan,telepon,email,geolokasi,deskripsi,website -->
                
                    <div class="form-group row">
                        <label for="idDirektori" class="col-md-3">ID Direktori</label>
                        <div class="col-md-9">
                            <input type="text" name="idDirektori" id="idDirektori" class="form-control" readonly value="<?=$data['direktori']['idDirektori'];?>" >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="idKategori" class="col-md-3">Kategori</label>
                        <div class="col-md-9">
                            <select name="idKategori" id="idKategori" class="form-control" >
                                <?php $i=0;  foreach($data['kategori'] AS $kat): ?>
                                    <option value="<?=$kat['idKategori'];?>"><?=$kat['namaKategori'];?></option>
                                <?php $i++; endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="idLembaga" class="col-md-3">Lembaga Perilis</label>
                        <div class="col-md-9">
                            <select name="idLembaga" id="idLembaga" class="form-control">
                                <?php $i=0;  foreach($data['lembaga'] AS $lbg): ?>
                                    <option value="<?=$lbg['idLembaga'];?>"><?=$lbg['namaLembaga'];?></option>
                                <?php $i++; endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="namaObjek" class="col-md-3">Nama Objek Direktori</label>
                        <div class="col-md-9">
                            <input type="text" name="namaObjek" id="namaObjek" class="form-control" value="<?=$data['direktori']['namaObjek'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="alamat" class="col-md-3">Alamat</label>
                        <div class="col-md-9">
                            <input type="text" name="alamat" id="alamat" class="form-control" value="<?=$data['direktori']['alamat'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="kecamatan" class="col-md-3">Kecamatan</label>
                        <div class="col-md-9">
                            <input type="text" name="kecamatan" id="kecamatan" class="form-control" value="<?=$data['direktori']['kecamatan'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="geolokasi" class="col-md-3">Koordinat Peta</label>
                        <div class="col-md-9">
                            <input type="text" name="geolokasi" id="geolokasi" class="form-control" value="<?=$data['direktori']['geolokasi'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="telepon" class="col-md-3">Telepon</label>
                        <div class="col-md-9">
                            <input type="text" name="telepon" id="telepon" class="form-control" value="<?=$data['direktori']['telepon'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-3">Email</label>
                        <div class="col-md-9">
                            <input type="email" name="email" id="email" class="form-control"  value="<?=$data['direktori']['email'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="website" class="col-md-3">Website</label>
                        <div class="col-md-9">
                            <input type="text" name="website" id="website" class="form-control"  value="<?=$data['direktori']['website'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="deskripsi" class="col-md-3">Deskripsi</label>
                        <div class="col-md-9">
                            <textarea name="deskripsi" id="deskripsi" class="form-control deskripsi" rows="15"> <?=$data['direktori']['deskripsi'];?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="dir_submit" class="col-md-3 bg-warning">Cek Data</label>
                        <div class="col-md-9 text-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>