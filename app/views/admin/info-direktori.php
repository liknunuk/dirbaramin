<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary text-light text-center">
                <p class="h2">DATA DIREKTORI</p>
                <?php Alert::sankil(); ?>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <th>ID Direktori</th>
                            <td><?=$data['direktori']['idDirektori'];?></td>
                        </tr>
                        <tr>
                            <th>Kategori</th>
                            <td><?=$data['direktori']['namaKategori'];?></td>
                        </tr>
                        <tr>
                            <th>Lembaga Perilis</th>
                            <td><?=$data['direktori']['namaLembaga'];?></td>
                        </tr>
                        <tr>
                            <th>Nama Direktori</th>
                            <td><?=$data['direktori']['namaObjek'];?></td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>
                                <?=$data['direktori']['alamat'].', Kecamatan '.$data['direktori']['kecamatan'];?><br>
                                Koordinat Peta : <?=$data['direktori']['geolokasi'];?>
                            </td>
                        </tr>
                        <tr>
                            <th>Kontak</th>
                            <td>
                                    Telp:<?=$data['direktori']['telepon'];?><br>
                                    Email:<?=$data['direktori']['email'];?><br>
                                    Website:<?=$data['direktori']['website'];?>
                            </td>
                        </tr>
                        <tr>
                            <th>Deskripsi Objek</th>
                            <td><?=$data['direktori']['deskripsi'];?></td>
                        </tr>
                        <tr>
                            <th>Ikon Objek</th>
                            <td>
                            <?php
                                if(empty($data['gambar'])){
                                    echo "<a class='btn btn-primary' href='".BASEURL."Admin/setIcon/direktori/{$data[direktori][idDirektori]}/dirinfo'>Set Icon</a>";
                                }else{
                                    echo '<img width="90px" height="90px" src="data:'.$data['gambar']['imgType'].';base64,' . $data['gambar']['iconImg'] . '" />';
                                    echo "<br>";
                                    echo "<a class='btn btn-primary' href='".BASEURL."Admin/setIcon/direktori/{$data['direktori']['idDirektori']}/dirinfo/chg'>Ganti Icon</a>";
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>