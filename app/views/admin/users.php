<div class="row">
    <div class="col-md-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary text-light text-center">
                <p class="h4">DAFTAR USER PENGELOLA</p>
                <?php Alert::sankil(); ?>
            </div>
            <div class="card-body table-responsive">
                <a href="<?=BASEURL;?>Admin/fruser" class="btn btn-primary">User Baru</a>
                <table class="table table-striped table-sm">
                    <thead>
                        <tr class='bg-dark text-light text-center'>
                            <th>ID</th>
                            <th>Nama Lengkap</th>
                            <th>Nama Pengguna</th>
                            <th>Lembaga</th>
                            <th>
                                <img src="<?=BASEURL;?>img/admin-control.png" alt="Kontrol" width="20px">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data['user'] AS $user): ?>
                        <tr>
                            <td><?=$user['userId'];?></td>
                            <td><?=$user['namaLengkap'];?></td>
                            <td><?=$user['userName'];?></td>
                            <td><?=$user['namaLembaga'];?></td>
                            <td width='180' align='center'>
                                <a href="<?=BASEURL;?>Admin/fruser/<?=$user['userId'];?>" class='btn btn-default'>
                                    <img src="<?=BASEURL;?>img/admin-edit.png" alt="info" style="width:24px; height:24px;">
                                </a>|
                                <a href="<?=BASEURL;?>Admin/usrfired/<?=$user['userId'];?>" class='btn btn-default'>
                                    <img src="<?=BASEURL;?>img/admin-fired.png" alt="info" style="width:24px; height:24px;">
                                </a>|
                                <a href="<?=BASEURL;?>Admin/usrcreds/<?=$user['userId'];?>" class='btn btn-default'>
                                    <img src="<?=BASEURL;?>img/admin-pass.png" alt="info" style="width:24px; height:24px;">
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Admin/users/1">1</a></li>
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Admin/users/2">2</a></li>
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Admin/users/3">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready(function(){
    let pn = <?=$data['pn'];?>;
    let id = pn-1;
    $('li.page-item').eq(pn).addClass('active');
})
</script>