<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary text-light text-center">
                <p class="h2">Data Direktori</p>
                <?php Alert::sankil(); ?>
            </div>
            <div class="card-body table-responsive">
            <div class="row mb-1">
                <div class="col-md-7">
                    <a href="<?=BASEURL;?>Admin/frdirektori" class="btn btn-primary">Direktori Baru</a>
                </div>
                <div class="col-md-4">
                    <input type="text" id="dirObjName" class="form-control" placeholder="Cari Nama Objek Direktori">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary" id="dirsearch">Cari</button>
                </div>
            </div>
                <table class="table table-sm table-bordered table-striped">
                    <thead class='bg-dark text-light text-center'>
                        <tr>
                            <th>ID</th>
                            <th>Kategori</th>
                            <th>Perilis</th>
                            <th>Nama Objek</th>
                            <th>Alamat</th>
                            <th>Kontak</th>
                            <th width='120px'>
                                <img src="<?=BASEURL;?>img/admin-control.png" alt="Kontrol" width="20px">
                            </th>
                        </tr>
                    </thead>
                    <tbody id="dirList">
                        <?php foreach($data['direktori'] AS $dir): ?>
                            <?php
                                // $desk = explode(" ",$dir['desk']);
                                // $rink = '';
                                // for($i = 0 ; $i < 3 ; $i ++ ){
                                //     $rink.=$desk[$i] . ' ';
                                // }
                                // $rink.=' <i class="text-info">...selanjutya</i> ';
                            ?>
                            <tr>
                                <td><?=$dir['idDirektori'];?></td>
                                <td><?=$dir['namaKategori'];?></td>
                                <td><?=$dir['namaLembaga'];?></td>
                                <td><?=$dir['namaObjek'];?><br><!-- ?=$rink;? --></td>
                                <td>
                                    <?=$dir['alamat'] .', '.$dir['kecamatan'] ;?><br>
                                    Lokasi:<?=$dir['geolokasi'];?>
                                </td>
                                <td>
                                    Telp:<?=$dir['telepon'];?><br>
                                    Email:<?=$dir['email'];?><br>
                                    Website:<?=$dir['website'];?>
                                </td>
                                <td>
                                    <a href="<?=BASEURL;?>Admin/dirinfo/<?=$dir['idDirektori'];?>">
                                        <img src="<?=BASEURL;?>img/admin-info.png" alt="info" style="width:24px; height:24px;">
                                    </a>|
                                    <a href="<?=BASEURL;?>Admin/frdirektori/<?=$dir['idDirektori'];?>">
                                        <img src="<?=BASEURL;?>img/admin-edit.png" alt="info" style="width:24px; height:24px;">
                                    </a> |
                                    <a href="#">
                                        <img src="<?=BASEURL;?>img/admin-trash.png" alt="trash" style="width:24px; height:24px;" class='trash' id='<?=$dir['idDirektori'];?>'>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>     
                    </tbody>
                </table>
                
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        </li>
                        <?php for($i=1 ; $i <=20 ; $i++): ?>
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Admin/direktori/<?=$i;?>"><?=$i;?></a></li>
                        <?php endfor; ?>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready(function(){
    let pn = <?=$data['pn'];?>;
    let id = pn-1;
    $('li.page-item').eq(pn).addClass('active');

    $('.trash').click(function(){
        let id = $(this).prop('id');
        let tenan = confirm('Data Akan Dihapus');
        if( tenan == true ){
            $.post('<?=BASEURL;?>Home/Deletion/direktori/',{
                id: id
            },function(resp){
                //alert(resp);  
                location.reload();
            })
        }
    })

    $('#dirsearch').click(function(){
        let dirname = $('#dirObjName').val().replace(' ','-');
        $.getJSON('<?=BASEURL;?>Mobile/dirSrc/'+dirname , function(dirs){
            $('#dirList tr').remove();
            $.each(dirs , function(i,data){
                $('#dirList').append(`
                <tr>
                    <td>${data.idDirektori}</td>
                    <td>${data.namaKategori}</td>
                    <td>${data.namaLembaga}</td>
                    <td>${data.namaObjek}</td>
                    <td>
                        ${data.alamat},${data.kecamatan}<br>
                        Lokasi:${data.geolokasi}
                    </td>
                    <td>
                        Telp:${data.telepon}<br>
                        Email:${data.email}<br>
                        Website:${data.website}
                    </td>
                    <td>
                        <a href="<?=BASEURL;?>Admin/dirinfo/${data.idDirektori}">
                            <img src="<?=BASEURL;?>img/admin-info.png" alt="info" style="width:24px; height:24px;">
                        </a>|
                        <a href="<?=BASEURL;?>Admin/frdirektori/${data.idDirektori}">
                            <img src="<?=BASEURL;?>img/admin-edit.png" alt="info" style="width:24px; height:24px;">
                        </a> |
                        <a href="#">
                            <img src="<?=BASEURL;?>img/admin-trash.png" alt="trash" style="width:24px; height:24px;" class='trash' id='${data.idDirektori}'>
                        </a>
                    </td>
                </tr>                
                `)
            })
        })
    })
})
</script>