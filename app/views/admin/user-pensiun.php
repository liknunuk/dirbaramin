<div class="row">
    <div class="col-md-10">
        <p class="h2 text-center">Konfirmasi Penghapusan</p>
        <table class="table table-sm">
            <tbody>
                <tr>
                    <th>Nama Lengkap</th><td><?=$data['user']['namaLengkap'];?></td>
                </tr>
                <tr>
                    <th>Lembaga</th><td><?=$data['user']['namaLembaga'];?></td>
                </tr>
            </tbody>
        </table>
        <div class="bg-danger text-center text-light py-2">User akan <strong>dihapus</strong> Dari Sistem</div>
    </div>
    <div class="col-md-2">
        <a href="<?=BASEURL;?>Admin/usrfired/<?=$data['user']['userId']?>/true" class="btn btn-danger">Hapus!</a>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>