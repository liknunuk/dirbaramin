
<div class="row mb-1">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary">
                <p class="h2">LEMBAGA BARU</p>
            </div>
        </div>
        <div class="card-body">
            <form class="form-inline" method="post" action="<?=BASEURL;?>Admin/setLembaga">
                <input type="hidden" name="mod" value="<?=$data['mod'];?>">
                <label class="sr-only" for="idLembaga">ID</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                    <div class="input-group-text">ID</div>
                    </div>
                    <input type="text" class="form-control" id="idLembaga" name="idLembaga" placeholder="ID Lembaga" readonly value="<?=$data['lbg']['idLembaga'];?>">
                </div>
                
                <label class="sr-only" for="namaLembaga">NAMA</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                    <div class="input-group-text">Nama</div>
                    </div>
                    <input type="text" class="form-control" id="namaLembaga" name="namaLembaga" placeholder="Nama Lembaga" value="<?=$data['lbg']['namaLembaga'];?>" >
                </div>
                
                <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </form>
        </div>
    </div>
</div>
<div class="row mb-1">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary">
                <p class="h2">DAFTAR LEMBAGA</p>
                <?php Alert::sankil(); ?>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-sm table-striped">
                <thead class="bg-dark text-light text-center">
                    <tr>
                        <th>ID</th>
                        <th>Nama Lembaga</th>
                        <th class="text-center" style="width:100px;">
                            <img src="<?=BASEURL;?>img/admin-control.png" alt="Kontrol" width="20px">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['lembaga'] as $lem): ?>
                        <tr>
                            <td><?=$lem['idLembaga'];?></td>
                            <td><?=$lem['namaLembaga'];?></td>
                            <td class="text-center">
                                <a href="<?=BASEURL."Admin/lembaga/{$data['pn']}/{$lem['idLembaga']}";?>" >
                                    <img src="<?=BASEURL;?>img/admin-edit.png" alt="info" style="width:24px; height:24px;">
                                </a> |
                                <a href="#">
                                    <img src="<?=BASEURL;?>img/admin-trash.png" alt="trash" style="width:24px; height:24px;" class='trash' id='<?=$lem['idLembaga'];?>'>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready(function(){
    $('.trash').click(function(){
        let id = $(this).prop('id');
        let tenan = confirm('Data Akan Dihapus');
        if( tenan == true ){
            $.post('<?=BASEURL;?>Home/Deletion/lembaga/',{
                id: id
            },function(resp){
                //alert(resp);  
                location.reload();
            })
        }
    })
})
</script>