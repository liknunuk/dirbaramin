<div class="row">
    <div class="col-lg-12">
        <div class="card border border-dark">
            <div class="card-header bg-secondary">
                <p class="h2">BERITA BARU</p>
            </div>
            <div class="card-body">
                <form action="<?=BASEURL;?>Admin/setBerita" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="mod" value="<?=$data['mod'];?>">
                    <div class="form-group row">
                        <label for="idBerita" class="col-md-3">ID Berita</label>
                        <div class="col-md-9">
                            <input type="text" name="idBerita" id="idBerita" class="form-control" readonly value="<?=$data['berita']['idBerita'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tanggalTerbit" class="col-md-3">Tanggal Rilis</label>
                        <div class="col-md-9">
                            <input type="date" name="tanggalTerbit" id="tanggalTerbit" class="form-control" value=<?=date('Y-m-d');?> readonly  value="<?=$data['berita']['tanggalTerbit'];?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="idLembaga" class="col-md-3">Lembaga Perilis</label>
                        <div class="col-md-9">
                            <select name="idLembaga" id="idLembaga" class="form-control">
                                <?php foreach($data['lembaga'] AS $lbg): ?>
                                    <option value="<?=$lbg['idLembaga'];?>"
                                    <?php if($lbg['idLembaga'] == $data['berita']['idLembaga']) echo "selected"; ?>><?=$lbg['namaLembaga'];?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="judul" class="col-md-3">Judul Berita</label>
                        <div class="col-md-9">
                            <input type="text" name="judul" id="judul" class="form-control" value="<?=$data['berita']['judul'];?>">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="uraian" class="col-md-3">Uraian</label>
                        <div class="col-md-9">
                            <textarea type="text" name="uraian" id="uraian" class="form-control deskripsi" rows='13'><?=$data['berita']['uraian'];?></textarea>
                        </div>
                    </div>

                    <!--
                    <div class="form-group row">
                        <label for="brt_banner" class="col-md-3">Banner Berita</label>
                        <div class="col-md-9">
                            <input type="file" accept="png|jpg|jpeg|gif" name="brt_banner" id="brt_banner" class="form-control">
                        </div>
                    </div> -->

                    <div class="form-group row">
                        <label for="brt_submit" class="col-md-3 bg-warning">Periksa Data!</label>
                        <div class="col-md-9 text-right">
                            <input type="submit" name="brt_submit" id="brt_submit" class="btn btn-primary" value="Simpan">
                        </div>
                    </div>

                </form>
            </div>        
        </div>
    </div>
</div>
<?php $this->view('template/bs4js');?>
