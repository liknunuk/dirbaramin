<?php
    // header("Content-type: image/png");
    // echo ($data['img']['iconImg']);
    // var_dump ($data['img']['iconImg']);
    $image = imagecreatefromstring($data['img']['iconImg']); 

    ob_start(); //You could also just output the $image via header() and bypass this buffer capture.
    imagejpeg($image, null, 80);
    $data = ob_get_contents();
    ob_end_clean();
    echo '<img src="data:image/jpg;base64,' .  base64_encode($data)  . '" />';

?>