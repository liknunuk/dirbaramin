<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?= $data['title']; ?></title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="<?=BASEURL;?>/img/logo-idi.png" width="16px" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap 4 CDN -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- jquery-ui -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
  <!-- tynimce-cloud -->
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <!-- <script>tinymce.init({selector:'textarea.deskripsi'});</script> -->
  <script>
    tinymce.init({
    selector: "textarea.deskripsi",  // change this value according to your HTML
    plugins: "image",
    menubar: 'file edit insert view format table tools help'
    });
  </script>
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= BASEURL .'/css/admin.css'; ?>">
  <!-- Custom Script u/ restful API -->
  <!--script> var pusdata = "< ?=PUSDATA;?>"; var baseurl = "< ?=BASEURL;?>"; </script-->
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-10  bg-success text-light py-3">
        <h2 class='text-center'>DIREKTORI BANJARNEGARA - ADMIN PANEL</h2>
    </div>
    <div class="col-lg-2 bg-success text-center">
      <img src="<?=BASEURL?>img/beranda-users.png" alt="users" style="width:35px; margin-top: 15px; marigin-left: auto;">
      <p class="py-0 px-0 text-light"><?=$_SESSION['admin'];?></p>
    </div>
  </div>
  <div class="row mt-2">
    <div class="col-lg-2"><!-- sidebar -->
      <div class="list-group" id="main-menu">
        
        <li class="list-group-item">
            <a href="<?=BASEURL;?>Admin/agenda/1"><img src="<?=BASEURL?>img/beranda-agenda.png" alt="Agenda" >Agenda</a>
        </li>
        
        <li class="list-group-item">
            <a href="<?=BASEURL;?>Admin/berita/1"><img src="<?=BASEURL?>img/beranda-berita.png" alt="Berita" >Berita</a>
        </li>
        
        <li class="list-group-item">
            <a href="<?=BASEURL;?>Admin/kategori/1"><img src="<?=BASEURL?>img/beranda-category.png" alt="Berita" >Ketegori</a>
        </li>
        
        <li class="list-group-item">
            <a href="<?=BASEURL;?>Admin/direktori/1"><img src="<?=BASEURL?>img/beranda-direktori.png" alt="Berita" >Direktori</a>
        </li>
        
        <li class="list-group-item">
            <a href="<?=BASEURL;?>Admin/lembaga/1"><img src="<?=BASEURL?>img/beranda-lembaga.png" alt="Berita" >Lembaga</a>
        </li>
        
        <li class="list-group-item">
            <a href="<?=BASEURL;?>Admin/users/1"><img src="<?=BASEURL?>img/beranda-users.png" alt="Berita" >Admin</a>
        </li>        

        <li class="list-group-item">
            <a href="<?=BASEURL;?>Admin/sitirahat"><img src="https://img.icons8.com/cotton/2x/logout-rounded-left.png" alt="Berita" >Keluar</a>
        </li>        
        
      </div>
    </div> <!-- sidebar -->
    <div class="col-lg-10"><!-- main content -->
 
