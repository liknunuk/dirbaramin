INSERT INTO direktori (idKategori,idLembaga,namaObjek,alamat,kecamatan,telepon) VALUES
(8,3,'SMP NEGERI 1 SUSUKAN','JL. RAYA SUSUKAN-BANJARNEGARA, PANERUSAN WETAN','Susukan',''),
(8,3,'SMP NEGERI 2 SUSUKAN','JLN. DESA GUMELEM WETAN, GUMELEM WETAN','Susukan',''),
(8,3,'SMP NEGERI 3 SUSUKAN','DERIK, DERIK','Susukan',''),
(8,3,'SMP NEGERI 4 SATAP SUSUKAN','JL. POGUNG, GUMELEM KULON','Susukan',''),
(8,3,'SMP PGRI SUSUKAN','JL. RAYA SUSUKAN - BANJARNEGARA, PANERUSAN KULON','Susukan',''),
(8,3,'SMP MUHAMMADIYAH PURWAREJA KLAMPOK','JL. K.H. AHMAD DAHLAN PURWAREJA KLAMPOK, PURWAREJA','Purwareja Klampok',''),
(8,3,'SMP NEGERI 1 PURWAREJA - KLAMPOK','JL. RAYA PURWAREJA - KLAMPOK , PURWAREJA','Purwareja Klampok',''),
(8,3,'SMP NEGERI 2 PURWOREJO KLAMPOK','JLN. JEND. A. YANI NO. 16, KLAMPOK','Purwareja Klampok',''),
(8,3,'SMP NEGERI 3 PURWOREJO KLAMPOK','JLN. RAYA TIMUR 343/51, KALIMANDI','Purwareja Klampok',''),
(8,3,'SMP PGRI PURWOREJO KLAMPOK','PURWOREJO KLAMPOK, KLAMPOK','Purwareja Klampok',''),
(8,3,'SMPIT MUTIARA HATI','DESA PURWAREJA, PURWAREJA KLAMPOK, PURWAREJA','Mandiraja',''),
(8,3,'SMP NEGERI 1 MANDIRAJA','JLN. RAYA MANDIRAJA KM. 19, MANDIRAJA KULON','Mandiraja',''),
(8,3,'SMP NEGERI 2 MANDIRAJA','PURWASABA, PURWASABA','Mandiraja',''),
(8,3,'SMP NEGERI 3 MANDIRAJA','MANDIRAJA, KALIWUNGU','Mandiraja',''),
(8,3,'SMP NEGERI 4 MANDIRAJA','KEC. MANDIRAJA, JALATUNDA','Mandiraja',''),
(8,3,'SMP NURUL AMBIYA MANDIRAJA','RABAK, SALAMERTA','Mandiraja',''),
(8,3,'SMP PLUS RIYADUL MUSTAQIM MANDIRAJA','KOMPLEKS MASJID AL-ISTIQOMAH BANTAR, KERTAYASA','Mandiraja',''),
(8,3,'SMP NEGERI 1 PURWANEGARA','JL. RAYA PURWANEGARA, KALIPELUS','Purwanegara',''),
(8,3,'SMP NEGERI 2 PURWANEGARA','JLN. HM SYATIBI MERDEN, MERDEN','Purwanegara',''),
(8,3,'SMP NEGERI 3 PURWONEGORO','JLN. RAYA PUCUNGBEDUG, PUCUNGBEDUG','Purwanegara',''),
(8,3,'SMP NEGERI 4 PURWONEGORO','JLN. RAYA MERDEN - KALITENGAH, KALITENGAH','Purwanegara',''),
(8,3,'SMP NEGERI 5 SATAP PURWANEGARA','KEC. PURWANEGARA, PETIR','Purwanegara',''),
(8,3,'SMP NEGERI 6 SATAP PURWANEGARA','RT 09 RW 04, KALIAJIR','Purwanegara',''),
(8,3,'SMP NEGERI 1 BAWANG','JLN. RAYA BAWANG, BLAMBANGAN','Bawang',''),
(8,3,'SMP NEGERI 2 BAWANG','PUCANG, PUCANG','Bawang',''),
(8,3,'SMP NEGERI 3 BAWANG','JLN. RAYA MAJALENGKA, MAJALENGKA','Bawang',''),
(8,3,'SMP NEGERI 4 SATU ATAP BAWANG','KEC. BAWANG, WINONG','Bawang',''),
(8,3,'SMP NEGERI 5 BAWANG','JLN. RAYA KEBONDALEM, KEBONDALEM','Bawang',''),
(8,3,'SMP COKROAMINOTO BANJARNEGARA','JALAN LETJEND. S. PARMAN NO. 61, PARAKANCANGGAH','Banjarnegara',''),
(8,3,'SMP IP BILINGUAL SCHOOL TUNAS BANGSA','KEC. BANJARNEGARA, PARAKANCANGGAH','Banjarnegara',''),
(8,3,'SMP ISLAM AL MUNAWWAROH','KUTABANJARNEGARA, KUTABANJARNEGARA','Banjarnegara',''),
(8,3,'SMP MUHAMMADIYAH BANJARNEGARA','JL. KH. AHMAD DAHLAN NO 6 BANJARNEGARA, KUTABANJARNEGARA','Banjarnegara',''),
(8,3,'SMP NEGERI 1 BANJARNEGARA','JLN. DIPAYUDA NO. 09, KUTABANJARNEGARA','Banjarnegara',''),
(8,3,'SMP NEGERI 2 BANJARNEGARA','JL. TENTARA PELAJAR NO. 31, KEC. BANJARNEGARA, KAB. BANJARNEGARA, SOKANANDI','Banjarnegara',''),
(8,3,'SMP NEGERI 4 BANJARNEGARA','JLN. SERMA MUKHLAS, KARANGTENGAH','Banjarnegara',''),
(8,3,'SMP NEGERI 5 BANJARNEGARA','JL. TENTARA PELAJAR NO 04 SOKANANDI KAB. BANJARNEGARA, SOKANANDI','Banjarnegara',''),
(8,3,'SMP NEGERI 6 SATU ATAP BANJARNEGARA','KEC. BANJARNEGARA, TLAGAWERA','Banjarnegara',''),
(8,3,'SMP TAMANSISWA BANJARNEGARA','JLN. MAYJEND. PANJAITAN NO. 29, KRANDEGAN','Banjarnegara',''),
(8,3,'SMP NEGERI 1 SIGALUH','SIGALUH, SIGALUH','Sigaluh',''),
(8,3,'SMP NEGERI 2 SATU ATAP SIGALUH','KEC. SIGALUH, PANAWAREN','Sigaluh',''),
(8,3,'SMP ISLAM SATRIA','SERED RT 02 RW 01, SERED','Madukara',''),
(8,3,'SMP ISLAM TERPADU PERMATA HATI BANJARNEGARA','JL. ABU BAKAR NO 11 RT 05 RW 01, PETAMBAKAN, PETAMBAKAN','Madukara',''),
(8,3,'SMP NEGERI 1 MADUKARA','JL. RAYA MADUKARA , TALUNAMBA','Madukara',''),
(8,3,'SMP NEGERI 2 MADUKARA','MADUKARA, KALIURIP','Madukara',''),
(8,3,'SMP NEGERI 3 BANJARNEGARA','JL. RAYA REJASA KM 3, REJASA','Madukara',''),
(8,3,'SMP COKROAMINOTO BANJARMANGU','JLN. RAYA KARANGKOBAR KM 15 BANJARMANGU - BANJARNEGARA, SIJERUK','Banjarmangu',''),
(8,3,'SMP ISLAM DARUNAJAH','JL. RAYA KABUPATEN LAMA BANJARKULON, BANJARKULON','Banjarmangu',''),
(8,3,'SMP NEGERI 1 BANJARMANGU','JLN. RAYA BANJARMANGU KM.8, KESENET','Banjarmangu',''),
(8,3,'SMP NEGERI 2 BANJARMANGU','JLN. RAYA PEKANDANGAN KM. 12, PEKANDANGAN','Banjarmangu',''),
(8,3,'SMP COKROAMINOTO WANADADI','JL. HOS COKROAMINOTO NO. 02 WANADADI, WANADADI','Wanadadi',''),
(8,3,'SMP MUHAMMADIYAH WANADADI','WANADADI, WANADADI','Wanadadi',''),
(8,3,'SMP NEGERI 1 WANADADI','JLN. RAYA TIMUR WANADADI, WANADADI','Wanadadi',''),
(8,3,'SMP NEGERI 2 WANADADI','JLN. RAYA TIMUR WANADADI KM 1, WANAKARSA','Wanadadi',''),
(8,3,'SMP MUHAMMADIYAH RAKIT','JL.TIMUR 100M PASAR, PINGIT','Rakit',''),
(8,3,'SMP NEGERI 1 RAKIT','JL. RAYA RAKIT, ADIPASIR','Rakit',''),
(8,3,'SMP NEGERI 2 RAKIT','LENGKONG, LENGKONG','Rakit',''),
(8,3,'SMP COKROAMINOTO PUNGGELAN','JL RAYA PURWASANA, PUNGGELAN BANJARNEGARA, PURWASANA','Punggelan',''),
(8,3,'SMP NEGERI 1 PUNGGELAN','JL. PASAR MANIS, PUNGGELAN','Punggelan',''),
(8,3,'SMP NEGERI 2 PUNGGELAN','JL. TANJUNGTIRTA, PUNGGELAN','Punggelan',''),
(8,3,'SMP NEGERI 3 PUNGGELAN','PUNGGELAN, PUNGGELAN','Punggelan',''),
(8,3,'SMP NEGERI 4 PUNGGELAN','DS. JEMBANGAN, JEMBANGAN','Punggelan',''),
(8,3,'SMP NEGERI 5 SATAP PUNGGELAN','MITJEN PURWASANA, PURWASANA','Punggelan',''),
(8,3,'SMP NEGERI 6 SATAP PUNGGELAN','DUSUN SIDAKARYA , MLAYA','Karangkobar',''),
(8,3,'SMP MA ARIF NU 01 KARANGKOBAR','JL. KH. HASYIM ASYARI GG FLAMBOYAN, LEKSANA','Karangkobar',''),
(8,3,'SMP NEGERI 1 KARANGKOBAR','KARANGKOBAR, LEKSANA','Karangkobar',''),
(8,3,'SMP NEGERI 2 KARANGKOBAR','JL. RAYA KARANGGONDANG-KARANGKOBAR, KARANGGONDANG','Karangkobar',''),
(8,3,'SMP NEGERI 3 SATAP KARANGKOBAR','KEC. KARANGKOBAR, BOLANG/SLATRI','Karangkobar',''),
(8,3,'SMP NEGERI 1 PAGENTAN','JALAN RAYA PAGENTAN NO. 94, PAGENTAN','Pagentan',''),
(8,3,'SMP NEGERI 2 PAGENTAN','JLN. ARIBAYA 2 PAGENTAN, ARIBAYA','Pagentan',''),
(8,3,'SMP NEGERI 3 PAGENTAN','MAJASARI, PAGENTAN','Pagentan',''),
(8,3,'SMP NEGERI 4 SATU ATAP PAGENTAN','KEC. PAGENTAN, BABADAN','Pagentan',''),
(8,3,'SMP NEGERI 5 PAGENTAN','KEC. PAGENTAN, KAREKAN','Pagentan',''),
(8,3,'SMP ISLAM AL MABRUR','JL. PON PES AL MABRUR GEMBOL, PEJAWARAN, BANJARNEGARA., GEMBOL','Pejawaran',''),
(8,3,'SMP NEGERI 1 PEJAWARAN','PEJAWARAN, PEJAWARAN','Pejawaran',''),
(8,3,'SMP NEGERI 2 PEJAWARAN','JL RAYA KARANGSARI KM 2 PEJAWARAN, KARANGSARI','Pejawaran',''),
(8,3,'SMP NEGERI 3 SATAP PEJAWARAN','RATAMBA RT 02/ RW 02 KEC. PEJAWARAN KAB, BANJARNEGARA, RATAMBA','Pejawaran',''),
(8,3,'SMP NEGERI 4 PEJAWARAN','KEC. PEJAWARAN, BEJI','Pejawaran',''),
(8,3,'SMP NEGERI 1 BATUR','JLN. RAYA BATUR, BATUR','Batur',''),
(8,3,'SMP NEGERI 2 BATUR','JLN. RAYA PEKASIRAN BATUR, PEKASIRAN','Batur',''),
(8,3,'SMP NEGERI 1 WANAYASA','WANAYASA, PESANTREN','Wanayasa',''),
(8,3,'SMP NEGERI 2 WANAYASA','WANAYASA, BANJARNEGARA, PANDANSARI','Wanayasa',''),
(8,3,'SMP NEGERI 3 WANAYASA','DUSUN PRINGWULUNG, DESA DAWUHAN RT 4 RW 3, KEC. WANAYASA 53457, DAWUHAN','Wanayasa',''),
(8,3,'SMP NEGERI 4 WANAYASA','KEC. WANAYASA, WANARAJA','Wanayasa',''),
(8,3,'SMP ISLAM AL MUBAROK','JL. RAYA KARANGGONDANG KALISAT KIDUL, KALISAT KIDUL','Kalibening',''),
(8,3,'SMP NEGERI 1 KALIBENING','JL. CATUR MARGA, KALIBENING','Kalibening',''),
(8,3,'SMP NEGERI 2 KALIBENING','JL. RAYA DESA KASINOMAN TELP. 08112611061, KASINOMAN','Kalibening',''),
(8,3,'SMP NEGERI 3 KALIBENING','JL. RAYA KALISAT KIDUL, KALISAT KIDUL','Kalibening',''),
(8,3,'SMP NEGERI 4 KALIBENING','JL. RAYA SEMBAWA KM 10, SEMBAWA','Kalibening',''),
(8,3,'SMP NEGERI 5 SATU ATAP KALIBENING','KEC. KALIBENING, ASINAN','Kalibening',''),
(8,3,'SMP NEGERI 6 SATU ATAP KALIBENING','KEC. KALIBENING, PLORENGAN','Kalibening',''),
(8,3,'SMP NEGERI 1 PANDANARUM','PANDANARUM, BEJI','Pandanarum',''),
(8,3,'SMP NEGERI 2 SATU ATAP PANDANARUM','JL. LAWEN PINGITLOR KM 6, PINGITLOR','Pandanarum',''),
(8,3,'SMP NEGERI 3 SATAP PANDANARUM','JL. KENDILWESI, LAWEN','Pandanarum',''),
(8,3,'SMP NEGERI 4 SATAP PANDANARUM','KEC. PANDANARUM, SIRONGGE','Pandanarum',''),
(8,3,'SMP COKROAMINOTO 1 PAGEDONGAN','JALAN WATUMAS KEBUTUHJURANG, KEBUTUHJURANG','Pagedongan',''),
(8,3,'SMP NEGERI 1 PAGEDONGAN','PAGEDONGAN, PAGEDONGAN','Pagedongan',''),
(8,3,'SMP NEGERI 2 SATU ATAP PAGEDONGAN','PAGEDONGAN, PAGEDONGAN','Pagedongan',''),
(8,3,'SMP NEGERI 3 PAGEDONGAN','JL. RAYA PESANGKALAN KM. 12, PESANGKALAN','Pagedongan','');

INSERT INTO direktori (idKategori,idLembaga,namaObjek,alamat,kecamatan,telepon) VALUES
(8,3,'MTSS MA`ARIF NU 01 SUSUKAN','JL. RAYA SUSUKAN KM 2 , PIASA WETAN','Susukan',''),
(8,3,'MTSS RIYADUSH SHOLIHIN PURWAREJA KLAMPOK','JL. PRAMUKA NO.556, PURWAREJA','Purwareja Klampok',''),
(8,3,'MTSS AL HIDAYAH PURWASABA','KOMPLEK PONDOK AL HIDAYAH PURWASABA, RT 01/05., PURWASABA','Purwareja Klampok',''),
(8,3,'MTSS AL MAARIF MANDIRAJA','JL. SUHADA NO.3, MANDIRAJA KULON','Mandiraja',''),
(8,3,'MTSS MUHAMMADIYAH MANDIRAJA','JL. RAYA MANDIRAJA, MANDIRAJA KULON','Mandiraja',''),
(8,3,'MTSS ATH THAHIRIYAH PUCUNGBEDUG','DESA PUCUNGBEDUG, PUCUNGBEDUG','Purwanegara',''),
(8,3,'MTSS MUHAMMADIYAH MERDEN','JL. DEMANG JIWAYUDA,MERDEN PURWANEGARA, MERDEN','Purwanegara',''),
(8,3,'RAUDLOTUT THOLIBIN PURWANEGARA','JL. RAYA PURWANEGARA KM. 15, -','Purwanegara',''),
(8,3,'MTSS ATH THAHIRIYAH PUCUNGBEDUG','DESA PUCUNGBEDUG, PUCUNGBEDUG','Purwanegara',''),
(8,3,'MTSS MUHAMMADIYAH MERDEN','JL. DEMANG JIWAYUDA,MERDEN PURWANEGARA, MERDEN','Purwanegara',''),
(8,3,'MTSS MUHAMMADIYAH BAWANG','JL. RAYA BANDINGAN KM 06, BAWANG','Bawang',''),
(8,3,'MTSS TANBIHUL GHOFILIN','DS. MANTRIANOM, MANTRIANOM','Bawang',''),
(8,3,'MTSN 1 BANJARNEGARA','JALAN RAYA SEMAMPIR NO.01 BANJARNEGARA, SEMAMPIR','Banjarnegara',''),
(8,3,'MTSN 2 BANJARNEGARA','JL. TENTARA PELAJAR KM.5 SOKANANDI, BANJARNEGARA, SOKANANDI','Banjarnegara',''),
(8,3,'MTSS AL FATAH','JL. S PARMAN KM. 3, PARAKANCANGGAH','Banjarnegara',''),
(8,3,'MTSS ANDALUSIA','JALAN LAPANGAN KRIDA REMAJA SOKANANDI , SOKANANDI','Banjarnegara',''),
(8,3,'MTS WALISONGO','JL.RAYA SIGALUH KM 10 BANJARNEGARA, SIGALUH','Sigaluh',''),
(8,3,'MTSS MUHAMMADIYAH SIGALUH','DESA SAWAL RT 02 RW 01, SAWAL','Sigaluh',''),
(8,3,'MTSS COKROAMINOTO MADUKARA','JL. RAYA DAWUHAN-MADUKARA KM.03 MADUKARA BANJARNEGARA, DAWUHAN','Madukara',''),
(8,3,'MTSS MUHAMMADIYAH PETAMBAKAN','JL. RAYA PETAMBAKAN KM. 3, PETAMBAKAN','Madukara',''),
(8,3,'MTSS MUHAMMADIYAH BANJARMANGU','JL. RAYA BANJARMANGU - BANJARKULON, BANJARMANGU','Banjarmangu',''),
(8,3,'MTSS COKROAMINOTO WANADADI','JL. RAYA TIMUR KM 01 WANADADI, WANADADI','Wanadadi',''),
(8,3,'MTSN 3 BANJARNEGARA','JL. RAYA NO.143 RAKIT, RAKIT','Rakit',''),
(8,3,'MTSN 4 BANJARNEGARA','JL. RAYA LENGKONG NO 60, RAKIT','Rakit',''),
(8,3,'MTSS AL MAARIF RAKIT','JL. GAJAH LAYANG RAKIT, RAKIT','Rakit',''),
(8,3,'MTSS COKROAMINOTO BADAMITA','JL. RAYA DESA BADAMITA, BADAMITA','Rakit',''),
(8,3,'MTSS COKROAMINOTO TANJUNGTIRTA','JLN. RAYA SIWARU NO. 4, TANJUNGTIRTA','Punggelan',''),
(8,3,'MTSS COKROAMINOTO TRIBUANA','JLN. RAYA TRIBUANA, KECAMATAN PUNGGELAN, KABUPATEN BANJARNEGARA, TRIBUANA','Punggelan',''),
(8,3,'MTSS MUHAMMADIYAH KECEPIT','JL. RAYA KECEPIT, PUNGGELAN, KECEPIT','Karangkobar',''),
(8,3,'MTSS MUHAMMADIYAH KARANGKOBAR','JL. GAYAM NO. 45 KARANGKOBAR BANJARNEGARA, KARANGKOBAR','Karangkobar',''),
(8,3,'MTSS MUHAMMADIYAH SARWODADI','JL. KARANGSARI-SARWODADI KM. 01, SARWODADI','Pejawaran',''),
(8,3,'MTSS RAUDLATUTHOLIBIN SEMANGKUNG','SEMANGKUNG, SEMANGKUNG','Pejawaran',''),
(8,3,'MTSS MUHAMMADIYAH BATUR','JL. GONDANG NO. 35 BATUR BANJARNEGARA 53456, BATUR','Batur',''),
(8,3,'MTS MAARIF KUBANG','JL. MASJID AT TAQWA NO.1 JOMBLANG DS. KUBANG, WANAYASA','Wanayasa',''),
(8,3,'MTSS MAARIF JATILAWANG','JL. RAYA WANAYASA BATUR KM 4, JATILAWANG','Wanayasa',''),
(8,3,'MTSS MUHAMMADIYAH WANAYASA','JL. RAYA WANAYASA-KALIBENING KM.01, WANAYASA','Wanadadi',''),
(8,3,'MTSS MUHAMMADIYAH 02 KALIBENING','JL. MASJID AMALIYAH NO 1, KALISAT KIDUL','Kalibening',''),
(8,3,'MTSS MUHAMMADIYAH 1 KALIBENING','JL. RAYA KALIBENING (UTARA TERMINAL) 53458, KALIBENING','Kalibening',''),
(8,3,'MTSS AL HIDAYAH TWELAGIRI','JLN. RAYA PAGEDONGAN, TWELAGIRI','Pagedongan',''),
(8,3,'MTSS COKROAMINOTO LEBAKWANGI','SILEGI RT.01/RW.03, DESA LEBAKWANGI, LEBAKWANGI','Pagedongan',''),
(8,3,'MTSS MAARIF AL IRSYAD','JL. RAYA GUNUNG JATI, GUNUNGJATI','Pagedongan',''),
(8,3,'MTSS NURUL HUDA PAGEDONGAN','JALAN RAYA PAGEDONGAN KM 09 BANJARNEGARA, PAGEDONGAN','Pagedongan','');

INSERT INTO direktori (idKategori,idLembaga,namaObjek,alamat,kecamatan,telepon) VALUES
(8,3,'SMAN 1 PURWAREJA KLAMPOK','JL.RAYA PURWAREJA KLAMPOK, PURWAREJA','',''),
(8,3,'SMAS PGRI PURWAREJA KLAMPOK','JL. PEKIRINGAN KLAMPOK, KLAMPOK','',''),
(8,3,'SMAS MA ARIF MANDIRAJA','JL. RAYA KERTAYASA KM 3 - MANDIRAJA, KERTAYASA','',''),
(8,3,'SMAN 1 PURWANEGARA','JL. RAYA PURWANEGARA, PURWANEGARA','',''),
(8,3,'SMAN 1 BAWANG','JL. RAYA PUCANG NO. 134, BAWANG, BAWANG','',''),
(8,3,'SMAN 1 BANJARNEGARA','JL. LETJEND. SUPRAPTO NO. 93 A, KUTABANJARNEGARA','',''),
(8,3,'SMAS COKROAMINOTO 1 BANJARNEGARA','JL. PEMUDA NO. 63, SEMARANG','',''),
(8,3,'SMAS MUHAMMADIYAH 1 BANJARNEGARA','JL. PEMUDA 61 A, SEMARANG','',''),
(8,3,'SMAN 1 SIGALUH','JL. RAYA SIGALUH KM.13 SIGALUH, GEMBONGAN','',''),
(8,3,'SMAN 1 WANADADI','JL. RAYA TAPEN-WANADADI, TAPEN','',''),
(8,3,'SMAN 1 KARANGKOBAR','JL. LEKSANA NO. 25 KARANGKOBAR, LEKSANA','',''),
(8,3,'SMAN 1 BATUR','JL. RAYA BATUR NO. 46A, BATUR','',''),
(8,3,'SMAS MUHAMMADIYAH 4 BANJARNEGARA','JL. RAYA KALIBENING NO. 75, KALIBENING','','');

INSERT INTO direktori (idKategori,idLembaga,namaObjek,alamat,kecamatan,telepon) VALUES
(8,3,'SMK NEGERI 1 SUSUKAN','JL. RAYA SUSUKAN - BRENGKOK, KEC. SUSUKAN, KAB. BANJARNEGARA, BRENGKOK','Susukan',''),
(8,3,'SMK KESEHATAN BHAKTI HUSADA','MANDIRAJA, KECITRAN','',''),
(8,3,'SMKS BINA MANDIRI PURWAREJA KLAMPOK','RAYA PURWAREJA KLAMPOK, KALIWINASUH','',''),
(8,3,'SMKS HKTI 1 PURWAREJA KLAMPOK','JL. RAYA PURWAREJA KLAMPOK NO. 82, PURWAREJA','',''),
(8,3,'SMKS HKTI 2 PURWAREJA KLAMPOK','JL. RAYA PURWAREJA KLAMPOK NO 82-B, PURWAREJA','',''),
(8,3,'SMKN 1 MANDIRAJA','JL. RAYA GLEMPANG, GLEMPANG','',''),
(8,3,'SMKN 1 BAWANG','JL. RAYA PUCANG NO. 132, BAWANG, BANJARNEGARA, JAWA TENGAH, PUCANG','',''),
(8,3,'SMKN 2 BAWANG','JL. RAYA MANTRIANOM NO. 75, BANDINGAN','',''),
(8,3,'SMKS AL FATAH BANJARNEGARA','JL. LET. JEND. S. PARMAN KM. 3, PARAKANCANGGAH','',''),
(8,3,'SMKS COKROAMINOTO 1 BANJARNEGARA','JL. PEMUDA NO. 63 BANJARNEGARA, SEMARANG','',''),
(8,3,'SMKS COKROAMINOTO 2 BANJARNEGARA','JL. LET. JEND. SOEPRAPTO 221, WANGON','',''),
(8,3,'SMKS MUHAMMADIYAH BANJARNEGARA','JL. LET. JEND. SUPRAPTO NO. 117 C, KUTABANJARNEGARA','',''),
(8,3,'SMKS PANCA BHAKTI BANJARNEGARA','JL. TENTARA PELAJAR KM 5 BANJARNEGARA, SOKANANDI','',''),
(8,3,'SMKS TAMANSISWA BANJARNEGARA','JL. MAYJEND PANJAITAN NO. 29, KRANDEGAN','',''),
(8,3,'SMKS MIFTAHUSSHOLIHIN SIGALUH','JL. SIGALUH KM 10 BANJARNEGARA, GEMBONGAN','',''),
(8,3,'SMKS DARUNNAJAH BANJARNEGARA','JL. RAYA KARANGKOBAR KM. 07, BANJARMANGU, BANJARNEGARA, KESENET','',''),
(8,3,'SMKS COKROAMINOTO WANADADI','H.O.S. COKROAMINOTO 2 WANADADI, WANADADI','',''),
(8,3,'SMKS PANCA BHAKTI RAKIT','RAKIT, ADIPASIR','',''),
(8,3,'SMKN 1 PUNGGELAN','JL. RAYA PASAR MANIS, LOJI, PUNGGELAN','',''),
(8,3,'SMKS MAARIF 01 KARANGKOBAR','K.H. HASYIM ASYARI, LEKSANA','',''),
(8,3,'SMKN 1 PEJAWARAN','PEJAWARAN, PENUSUPAN','',''),
(8,3,'SMKS AL MABRUR PEJAWARAN','JL. RAYA DIENG BATUR, GEMBOL','',''),
(8,3,'SMKN 1 WANAYASA','JL. RAYA WANAYASA, WANAYASA','',''),
(8,3,'SMK MAARIF NU 01 AL MUBAROK','JL. KALISATKIDUL, KALIBENING, BANJARNEGARA, KALISAT KIDUL','',''),
(8,3,'SMP-SMK NEGERI SATU ATAP PANDANARUM','JL. RAYA SIRONGGE PRINGAMBA PANDANARUM BANJARNEGARA, PRINGAMBA','','');

INSERT INTO direktori (idKategori,idLembaga,namaObjek,alamat,kecamatan,telepon) VALUES
(8,3,'MAS AL HIDAYAH 1 PURWAREJA KLAMPOK','JL. PRAMUKA NO. 556 PURWAREJA RT 01 RW 08, PURWAREJA','',''),
(8,3,'MAN 1 BANJARNEGARA','JL. RAYA PUCANG KM. 03 BANJARNEGARA, PUCANG','',''),
(8,3,'MAS MAARIF BAWANG','JL. MASJID NO. 1, WIRAMASTRA','',''),
(8,3,'MAS TANBIHUL GHOFILIN','JL. RAYA MANTRIANOM KM. 07 , BAWANG','',''),
(8,3,'MAN 2 BANJARNEGARA','JL. LETJEND SUPRAPTO 95A, KUTABANJARNEGARA','',''),
(8,3,'MAS AL FATAH','JL. S. PARMAN KM. 3 BANJARNEGARA, PARAKANCANGGAH','',''),
(8,3,'MAS MUHAMMADIYAH SIGALUH','SAWAL RT 03/01, SAWAL','',''),
(8,3,'MAS WALISONGO','JL. RAYA SIGALUH RT 02 RW 05 KM.10 BANJARNEGARA, GEMBONGAN','',''),
(8,3,'MAS TERPADU ASADIYAH','JL. SOKAWERA DESA SERED RT. 02 RW. 01, SERED','',''),
(8,3,'MAS COKROAMINOTO WANADADI','JL. RAYA TIMUR KM 1. WANADADI-BANJARNEGARA, WANADADI','',''),
(8,3,'MAS GUPPI RAKIT','JL.RAYA RAKIT NO.137, RAKIT','',''),
(8,3,'MAS COKROAMINOTO KARANGKOBAR','JL.KARANGKOBAR KM.1 KARANGKOBAR-BANJARNEGARA, KARANGKOBAR','',''),
(8,3,'MAS MA`ARIF NU WANAYASA','JL SIRANDU NO.02 RT 4/ 2, KUBANG','',''),
(8,3,'MAS NURUL HIKMAH KALIBENING','JL. DESA KALISATKIDUL, KALISAT KIDUL','',''),
(8,3,'MAS COKROAMINOTO PAGEDONGAN','JLN. RAYA SILEGI LEBAKWANGI PAGEDONGAN, LEBAKWANGI','',''),
(8,3,'MAS MAARIF AL IRSYAD','GUNUNG JATI RT. 03 RW. 02, GUNUNGJATI','','');

