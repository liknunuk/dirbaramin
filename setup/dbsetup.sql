-- MySQL dump 10.17  Distrib 10.3.22-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: direbara
-- ------------------------------------------------------
-- Server version	10.3.22-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda` (
  `idAgenda` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `idLembaga` int(2) unsigned DEFAULT NULL,
  `namaAgenda` tinytext DEFAULT NULL,
  `tanggalMulai` date DEFAULT NULL,
  `tanggalAkhir` date DEFAULT NULL,
  `lokasi` tinytext DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `kontakPerson` varchar(30) DEFAULT NULL,
  `kontakAddress` tinytext DEFAULT NULL,
  PRIMARY KEY (`idAgenda`),
  KEY `fkAgdLembaga` (`idLembaga`),
  CONSTRAINT `fkAgdLembaga` FOREIGN KEY (`idLembaga`) REFERENCES `lembaga` (`idLembaga`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `berita`
--

DROP TABLE IF EXISTS `berita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `berita` (
  `idBerita` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `idLembaga` int(2) unsigned DEFAULT NULL,
  `judul` tinytext DEFAULT NULL,
  `uraian` text DEFAULT NULL,
  `tanggalTerbit` date DEFAULT NULL,
  PRIMARY KEY (`idBerita`),
  KEY `fkBrtLembaga` (`idLembaga`),
  CONSTRAINT `fkBrtLembaga` FOREIGN KEY (`idLembaga`) REFERENCES `lembaga` (`idLembaga`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `direktori`
--

DROP TABLE IF EXISTS `direktori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direktori` (
  `idDirektori` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `idKategori` int(2) unsigned DEFAULT NULL,
  `idLembaga` int(2) unsigned DEFAULT NULL,
  `namaObjek` tinytext DEFAULT NULL,
  `alamat` tinytext DEFAULT NULL,
  `kecamatan` varchar(20) DEFAULT NULL,
  `telepon` varchar(14) DEFAULT NULL,
  `email` tinytext DEFAULT NULL,
  `geolokasi` varchar(30) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `website` tinytext DEFAULT NULL,
  PRIMARY KEY (`idDirektori`),
  KEY `fkDirKtg` (`idKategori`),
  KEY `fkDirLembaga` (`idLembaga`),
  CONSTRAINT `fkDirKtg` FOREIGN KEY (`idKategori`) REFERENCES `kategori` (`idKategori`) ON DELETE CASCADE,
  CONSTRAINT `fkDirLembaga` FOREIGN KEY (`idLembaga`) REFERENCES `lembaga` (`idLembaga`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=519 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `iconDefcat`
--

DROP TABLE IF EXISTS `iconDefcat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iconDefcat` (
  `idKategori` int(2) unsigned DEFAULT NULL,
  `defIcon` mediumblob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `iconImages`
--

DROP TABLE IF EXISTS `iconImages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iconImages` (
  `idImage` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `tableSrc` enum('agenda','berita','kategori','direktori','none') DEFAULT 'none',
  `tableIdx` int(5) unsigned NOT NULL DEFAULT 0,
  `imgType` varchar(20) DEFAULT 'image/png',
  `iconImg` mediumblob DEFAULT NULL,
  PRIMARY KEY (`idImage`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `idKategori` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `idLembaga` int(2) unsigned DEFAULT NULL,
  `namaKategori` tinytext DEFAULT NULL,
  PRIMARY KEY (`idKategori`),
  KEY `fkCatLembaga` (`idLembaga`),
  CONSTRAINT `fkCatLembaga` FOREIGN KEY (`idLembaga`) REFERENCES `lembaga` (`idLembaga`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lembaga`
--

DROP TABLE IF EXISTS `lembaga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lembaga` (
  `idLembaga` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `namaLembaga` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`idLembaga`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userAdmin`
--

DROP TABLE IF EXISTS `userAdmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userAdmin` (
  `userId` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) NOT NULL,
  `userPassword` varchar(32) NOT NULL,
  `namaLengkap` varchar(40) DEFAULT NULL,
  `idLembaga` int(2) unsigned DEFAULT NULL,
  PRIMARY KEY (`userId`),
  KEY `fkAdmLembaga` (`idLembaga`),
  CONSTRAINT `fkAdmLembaga` FOREIGN KEY (`idLembaga`) REFERENCES `lembaga` (`idLembaga`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-28 20:25:54
